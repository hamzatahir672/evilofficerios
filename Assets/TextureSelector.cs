﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureSelector : MonoBehaviour
{
    [SerializeField] Material[] texturePacks;
    
    MeshRenderer _mRenderer;
    SkinnedMeshRenderer _smRenderer;

    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<MeshRenderer>() != null) 
        {
            _mRenderer = GetComponent<MeshRenderer>();
            _mRenderer.material = texturePacks[PlayerPrefs.GetInt("TextureIndex", 0)];
            return;
        }

        if (GetComponent<SkinnedMeshRenderer>() != null)
        {
            _smRenderer = GetComponent<SkinnedMeshRenderer>();
            _smRenderer.material = texturePacks[PlayerPrefs.GetInt("TextureIndex", 0)];
            return;
        }
    }
}
