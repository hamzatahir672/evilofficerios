﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class FeedbackManager : MonoBehaviour {
    [SerializeField] private TMPro.TextMeshProUGUI txtData;
    [SerializeField] private TMPro.TMP_InputField inputField;
    [SerializeField] private UnityEngine.UI.Button btnSubmit;
    [SerializeField] private CollectionOption option;

    [SerializeField] private GameObject feedBackResultBox;
    [SerializeField] private UnityEngine.UI.Text feedBackResultText;

    private enum CollectionOption { openEmailClient, openGFormLink, sendGFormData };

    private const string kReceiverEmailAddress = "algieclever@gmail.com";

    private const string kGFormBaseURL = "https://docs.google.com/forms/d/e/1FAIpQLScZBsKU472IPgmtpVOrTLAWsnQcVM6M9xUiXZUyy6D27C6WzQ/";
    private const string kGFormEntryID = "entry.2107212114";
    
    void Start() {
        //UnityEngine.Assertions.Assert.IsNotNull( txtData );
        //UnityEngine.Assertions.Assert.IsNotNull( btnSubmit );
        btnSubmit.onClick.AddListener( delegate {
            switch ( option ) {
                case CollectionOption.openEmailClient:
                    OpenEmailClient( txtData.text );
                    break;
                case CollectionOption.openGFormLink:
                    OpenGFormLink();
                    break;
                case CollectionOption.sendGFormData:
                    StartCoroutine( SendGFormData( txtData.text +  " | Device :" + SystemInfo.deviceModel + " | Sent on : " + System.DateTime.Now.ToString() ) );
                    break;
            }
        } );
    }

    private static void OpenEmailClient( string feedback ) {
        Debug.Log("Opening EmailClient...");
        string email = kReceiverEmailAddress;
        string subject = "Feedback";
        string body = "<" + feedback + ">";
        OpenLink( "mailto:" + email + "?subject=" + subject + "&body=" + body );
    }

    // We cannot have spaces in links for iOS
    public static void OpenLink( string link ) {
        Debug.Log("Opening Link...");
        bool googleSearch = link.Contains( "google.com/search" );
        string linkNoSpaces = link.Replace( " ", googleSearch ? "+" : "%20");
        Application.OpenURL( linkNoSpaces );
    }

    private static void OpenGFormLink() {
        Debug.Log("Opening GFormLink...");
        string urlGFormView = kGFormBaseURL + "viewform";
        OpenLink( urlGFormView );
    }

    private IEnumerator SendGFormData<T>( T dataContainer ) {
        Debug.Log("Sending GFormData...");

        if (txtData.text.ToCharArray().Length > 1)
        {
            bool isString = dataContainer is string;
            string jsonData = isString ? dataContainer.ToString() : JsonUtility.ToJson(dataContainer);

            WWWForm form = new WWWForm();
            form.AddField(kGFormEntryID, jsonData);
            string urlGFormResponse = kGFormBaseURL + "formResponse";
            using (UnityWebRequest www = UnityWebRequest.Post(urlGFormResponse, form))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    ShowFeedBackMessage("Message not Sent! Please check your network");
                }
                else if (www.isHttpError)
                {
                    ShowFeedBackMessage("Internal Problem. Message Not Sent");
                }
                else 
                {
                    ShowFeedBackMessage("Message Sent! Thanks for your feedback");
                    inputField.text = "";
                }
            }
        }
        else 
        {
            yield return null;
            ShowFeedBackMessage("Empty Field! Please type something!");
        }
        
    }

    public void ShowFeedBackMessage(string message) 
    {
        StartCoroutine(coroutine_ShowFeedbackMessage(message));
    }

    IEnumerator coroutine_ShowFeedbackMessage(string _message) 
    {
        feedBackResultBox.SetActive(true);
        feedBackResultText.text = _message;

        yield return new WaitForSeconds(2f);

        feedBackResultBox.SetActive(false);
    }
}
