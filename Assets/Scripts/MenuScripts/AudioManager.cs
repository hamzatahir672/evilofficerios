﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else 
        {
            Destroy(this);
        }
    }

    AudioSource MainAudioSource;
    public float AudioVolume = 0.5f;
    public AudioClip[] clips;


    // Start is called before the first frame update
    void Start()
    {
        MainAudioSource = GetComponent<AudioSource>();
        AudioVolume = PlayerPrefs.GetFloat("AudioVolume", 0.5f);
        ChangeVolume();
        PlayClip(0);
    }

    public void ChangeVolume()
    {
        MainAudioSource.volume = AudioVolume;
        PlayerPrefs.SetFloat("AudioVolume", AudioVolume);
    }

    public void PlayClip(int clipIndex)
    {
        MainAudioSource.volume = AudioVolume;
        MainAudioSource.clip = clips[clipIndex];
        MainAudioSource.Play();
        //Debug.Log("Clip Played");
    }

    public void StopClip()
    {
        StartCoroutine(LerpVolume());
        Debug.Log("Clip Stopped");
    }

    IEnumerator LerpVolume()
    {
        while(MainAudioSource.volume > 0)
        {
            MainAudioSource.volume -= (Time.deltaTime / 2);
            yield return null;
        }
	}

    
}
