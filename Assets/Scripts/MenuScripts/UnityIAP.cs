﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.


// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class UnityIAP : MonoBehaviour
{
    public static UnityIAP instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    //    // public Text CoinNumbers;
    //    // public GameObject RemovesAdsUnit;
    //    // public gameManageMain manager;
    //    public static string RemoveAdsID = "com.evilofficer.removeads";
    //        public static string _1000CoinsID = "com.evilofficer.1000coin";
    //        public static string _3000CoinsID = "com.evilofficer.3000coin";
    //        public static string _4500CoinsID = "com.evilofficer.4500coin";
    public Text messageBoxText;
    public GameObject messageBox;

    //        [SerializeField] GameObject RemoveAdsBtn;

    //        private static IStoreController m_StoreController;          // The Unity Purchasing system.
    //        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    //        // Product identifiers for all products capable of being purchased: 
    //        // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
    //        // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
    //        // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    //        // General product identifiers for the consumable, non-consumable, and subscription products.
    //        // Use these handles in the code to reference which product to purchase. Also use these values 
    //        // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
    //        // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    //        // specific mapping to Unity Purchasing's AddProduct, below.
    //        //public static string ProductId ;   


    //        // Apple App Store-specific product identifier for the subscription product.
    //        private static string kProductNameAppleSubscription =  "com.unity3d.subscription.new";

    //        // Google Play Store-specific product identifier subscription product.
    //        private static string kProductNameGooglePlaySubscription =  "com.unity3d.subscription.original"; 

    //        void Start()
    //        { 
    //            // If we haven't set up the Unity Purchasing reference
    //            if (m_StoreController == null)
    //            {
    //                // Begin to configure our connection to Purchasing
    //                InitializePurchasing();
    //            }
    //        }

    //        public void InitializePurchasing() 
    //        {
    //            // If we have already connected to Purchasing ...
    //            if (IsInitialized())
    //            {
    //                // ... we are done here.
    //                return;
    //            }

    //            // Create a builder, first passing in a suite of Unity provided stores.
    //            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

    //            // Add a product to sell / restore by way of its identifier, associating the general identifier
    //            // with its store-specific identifiers.
    //            // builder.AddProduct("com.rmw.removeads", ProductType.Subscription, new IDs(){
    //            //     { kProductNameAppleSubscription, AppleAppStore.Name },
    //            //     { kProductNameGooglePlaySubscription, GooglePlay.Name },
    //            // });

    //            builder.AddProduct(RemoveAdsID, ProductType.Consumable);
    //            builder.AddProduct(_1000CoinsID, ProductType.Consumable);
    //            builder.AddProduct(_3000CoinsID, ProductType.Consumable);
    //            builder.AddProduct(_4500CoinsID, ProductType.Consumable);


    //            // Continue adding the non-consumable product.
    //            // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
    //            // if the Product ID was configured differently between Apple and Google stores. Also note that
    //            // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
    //            // must only be referenced here. 

    //            // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
    //            // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
    //            UnityPurchasing.Initialize(this, builder);
    //        }


    //        private bool IsInitialized()
    //        {
    //            // Only say we are initialized if both the Purchasing references are set.
    //            return m_StoreController != null && m_StoreExtensionProvider != null;
    //        }


    //        public void BuyProductRemoveAds()
    //        {
    //        // Buy the consumable product using its general identifier. Expect a response either 
    //        // through ProcessPurchase or OnPurchaseFailed asynchronously.
    //            BuyProductID(RemoveAdsID);
    //            //ShowMessage("Loading Purchase...");
    //        }

    //    public void BuyProduct1000Coin()
    //    {
    //        // Buy the consumable product using its general identifier. Expect a response either 
    //        // through ProcessPurchase or OnPurchaseFailed asynchronously.
    //        BuyProductID(_1000CoinsID);
    //        //ShowMessage("Loading Purchase...");
    //    }

    //    public void BuyProduct3000Coin()
    //    {
    //        // Buy the consumable product using its general identifier. Expect a response either 
    //        // through ProcessPurchase or OnPurchaseFailed asynchronously.
    //        BuyProductID(_3000CoinsID);
    //        //ShowMessage("Loading Purchase...");
    //    }

    //    public void BuyProduct4500Coin()
    //    {
    //        // Buy the consumable product using its general identifier. Expect a response either 
    //        // through ProcessPurchase or OnPurchaseFailed asynchronously.
    //        BuyProductID(_4500CoinsID);
    //        //ShowMessage("Loading Purchase...");
    //    }

    public void ShowMessage(string message, float displayTime = 3f)
    {
        StartCoroutine(coroutine_ShowMessage(message, displayTime));
    }

    IEnumerator coroutine_ShowMessage(string _message, float _displayTime)
    {
        messageBox.SetActive(true);
        messageBoxText.text = _message;

        yield return new WaitForSeconds(_displayTime);

        messageBox.SetActive(false);
    }


    //    void BuyProductID(string productId)
    //        {
    //            // If Purchasing has been initialized ...
    //            if (IsInitialized())
    //            {
    //                // ... look up the Product reference with the general product identifier and the Purchasing 
    //                // system's products collection.
    //                Product product = m_StoreController.products.WithID(productId);

    //                // If the look up found a product for this device's store and that product is ready to be sold ... 
    //                if (product != null && product.availableToPurchase)
    //                {
    //                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
    //                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
    //                    // asynchronously.
    //                    ShowMessage("Initializing Product...");
    //                    m_StoreController.InitiatePurchase(product);
    //                }
    //                // Otherwise ...
    //                else
    //                {
    //                    // ... report the product look-up failure situation  
    //                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
    //                    ShowMessage("Product not found!");
    //                }
    //            }
    //            // Otherwise ...
    //            else
    //            {
    //                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
    //                // retrying initiailization.
    //                Debug.Log("BuyProductID FAIL. Not initialized.");
    //                ShowMessage("Purchase failed! Error 2202");
    //            }
    //        }


    //        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    //        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    //        public void RestorePurchases()
    //        {
    //            // If Purchasing has not yet been set up ...
    //            if (!IsInitialized())
    //            {
    //                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
    //                Debug.Log("RestorePurchases FAIL. Not initialized.");
    //                return;
    //            }

    //            // If we are running on an Apple device ... 
    //            if (Application.platform == RuntimePlatform.IPhonePlayer || 
    //                Application.platform == RuntimePlatform.OSXPlayer)
    //            {
    //                // ... begin restoring purchases
    //                Debug.Log("RestorePurchases started ...");

    //                // Fetch the Apple store-specific subsystem.
    //                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
    //                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
    //                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
    //                apple.RestoreTransactions((result) => {
    //                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
    //                    // no purchases are available to be restored.
    //                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
    //                });
    //            }
    //            // Otherwise ...
    //            else
    //            {
    //                // We are not running on an Apple device. No work is necessary to restore purchases.
    //                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
    //            }
    //        }


    //        //  
    //        // --- IStoreListener
    //        //

    //        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    //        {
    //            // Purchasing has succeeded initializing. Collect our Purchasing references.
    //            Debug.Log("OnInitialized: PASS");

    //            // Overall Purchasing system, configured with products for this application.
    //            m_StoreController = controller;
    //            // Store specific subsystem, for accessing device-specific store features.
    //            m_StoreExtensionProvider = extensions;
    //        }


    //        public void OnInitializeFailed(InitializationFailureReason error)
    //        {
    //            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
    //            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    //            ShowMessage("IAP not initialized! Error 2201");
    //        }


    //        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
    //        {
    //            // A consumable product has been purchased by this user.
    //            if (String.Equals(args.purchasedProduct.definition.id, RemoveAdsID, StringComparison.Ordinal))
    //            {
    //                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
    //                ShowMessage("Success! Ads Removed");

    //                PlayerPrefs.SetString("NoAds", "1");
    //                RemoveAdsBtn.SetActive(false);

    //                // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
    //                // ScoreManager.score += 100;
    //            }
    //        else if (String.Equals(args.purchasedProduct.definition.id, _1000CoinsID, StringComparison.Ordinal))
    //        {
    //            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
    //            ShowMessage("Success! You got 1000 coins");

    //            MenuManager.instance.UpdateCoins(1000);

    //            // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
    //            // ScoreManager.score += 100;
    //        }
    //        else if (String.Equals(args.purchasedProduct.definition.id, _3000CoinsID, StringComparison.Ordinal))
    //        {
    //            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
    //            ShowMessage("Success! You got 3000 coins");

    //            MenuManager.instance.UpdateCoins(3000);

    //            // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
    //            // ScoreManager.score += 100;
    //        }
    //        else if (String.Equals(args.purchasedProduct.definition.id, _4500CoinsID, StringComparison.Ordinal))
    //        {
    //            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
    //            ShowMessage("Success! You got 4500 coins");

    //            MenuManager.instance.UpdateCoins(4500);

    //            // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
    //            // ScoreManager.score += 100;
    //        }

    //        else 
    //            {
    //                Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
    //                ShowMessage("Purchase failed! Error 2203");
    //            }

    //            // Return a flag indicating whether this product has completely been received, or if the application needs 
    //            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
    //            // saving purchased products to the cloud, and when that save is delayed. 
    //            return PurchaseProcessingResult.Complete;
    //        }


    //        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    //        {
    //            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
    //            // this reason with the user to guide their troubleshooting actions.
    //            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    //        }
}
