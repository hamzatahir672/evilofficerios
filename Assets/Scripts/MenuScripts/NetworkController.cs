﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using aitcHUtils;

public class NetworkController : MonoBehaviourPunCallbacks
{
    public static NetworkController instance;
    int temp;
    ExitGames.Client.Photon.Hashtable tempHash = new ExitGames.Client.Photon.Hashtable();

    private List<RoomInfo> totalRooms = new List<RoomInfo>();

    public List<RoomInfo> TotalRooms { get { return totalRooms; } }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    #region Room Custom Property Keys
    public string CP_ROOMCODE { get { return "RoomCode"; } }
    public string CP_GAMEVERSION { get { return "GameVersion"; } }
    public string CP_NEXTROOMCODE { get { return "NRCode"; } }
    public string CP_ISVISIBLE { get { return "IsVisible"; } }
    public string CP_NOVOTING { get { return "AllowVoting"; } }
    public string CP_NODOORSABOTAGE { get { return "AllowDoorSabotage"; } }
    public string CP_MAP { get { return "Map"; } }
    public string CP_GAMEMODE { get { return "GameMode"; } }
    public string CP_IMPOSTERS { get { return "Impostors"; } }
    public string CP_CONFIRMEJECT { get { return "ConfirmEject"; } }
    public string CP_ANONYMOUSVOTING { get { return "AnonymousVoting"; } }
    public string CP_0CREWMATEWIN { get { return "0CrewmateWin"; } }
    public string CP_EMERGENCYMEETINGCREWMATE { get { return "EmergencyMeetingCrewmate"; } }
    public string CP_EMERGENCYMEETINGIMPOSTOR { get { return "EmergencyMeetingImpostor"; } }
    public string CP_EMERGENCYCOOLDOWNCREWMATE { get { return "EmergencyCooldownCrewmate"; } }
    public string CP_EMERGENCYCOOLDOWNIMPOSTOR { get { return "EmergencyCooldownImpostor"; } }
    public string CP_DISCUSSIONTIME { get { return "DiscussionTime"; } }
    public string CP_VOTINGTIME { get { return "VotingTime"; } }
    public string CP_PLAYERSPEED { get { return "PlayerSpeed"; } }
    public string CP_IMPOSTORSPEED { get { return "ImpostorSpeed"; } }
    public string CP_CREWMATEVISION { get { return "CrewmateVision"; } }
    public string CP_IMPOSTORVISION { get { return "ImpostorVision"; } }
    public string CP_KILLCOOLDOWN { get { return "KillCooldown"; } }
    public string CP_KILLDISTANCE { get { return "KillDistance"; } }
    public string CP_TASKBARUPDATES { get { return "TaskBarUpdates"; } }
    public string CP_VISUALTASK { get { return "VisualTask"; } }
    public string CP_COMMONTASK { get { return "CommonTask"; } }
    public string CP_LONGTASK { get { return "LongTask"; } }
    public string CP_SHORTTASK { get { return "ShortTask"; } }
    public string CP_Venting { get { return "Venting"; } }
    public string CP_ALLOWKILLING { get { return "AllowKill"; } }
    public string CP_PLAYASPETS { get { return "PlayerAsPets"; } }
    public string CP_VOICECHAT { get { return "VoiceChat"; } }
    public string CP_HOSTISIMPOSTOR { get { return "HostIsImpostor"; } }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        //if (PlayerPrefs.GetInt("HasMatch", 0) == 1)
        //{
        //    PhotonNetwork.Reconnect();
        //    return;
        //}

        PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = PlayerPrefs.GetString("Region", "eu");

        PhotonNetwork.ConnectUsingSettings();


        //PhotonNetwork.ConnectToRegion("eu");
    }

    private void Update()
    {
        if (!PhotonNetwork.IsConnected && Application.internetReachability != NetworkReachability.NotReachable)
        {
            PhotonNetwork.Reconnect();
        }

        //Debug.LogError("IsConnected : " + PhotonNetwork.IsConnected + ". IsConnectedAndReady : " + PhotonNetwork.IsConnectedAndReady + ". InLobby : " + PhotonNetwork.InLobby);

        //Debug.Log(PhotonNetwork.IsConnectedAndReady);
    }

    public void ReconnectToServer()
    {
        StartCoroutine(coroutine_Reconnect());
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
    }

    public override void OnLeftLobby()
    {
        base.OnLeftLobby();
    }

    IEnumerator coroutine_Reconnect()
    {
        float connectionTimeout = 10f;
        float disconnectionTimeout = 10f;

        TryDisconnect();
        while (PhotonNetwork.IsConnected)
        {
            disconnectionTimeout -= Time.deltaTime;
            yield return null;
            if (disconnectionTimeout <= 0)
            {
                break;
            }
        }

        TryConnect();
        while (!PhotonNetwork.IsConnected)
        {
            connectionTimeout -= Time.deltaTime;
            yield return null;
            if (connectionTimeout <= 0)
            {
                //MenuManager.instance.ShowMessage("Connection timed out. Please check your connection and restart the game");
                break;
            }
        }
    }

    public void TryConnect()
    {
        if (!PhotonNetwork.IsConnected)
            PhotonNetwork.ConnectUsingSettings();
    }

    public void TryDisconnect()
    {
        if (PhotonNetwork.IsConnected)
            PhotonNetwork.Disconnect();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("We are now connected to " + PhotonNetwork.CloudRegion + " server!");

        //string uID;
        //if (PlayerPrefs.HasKey("UserId"))
        //{
        //    uID = PlayerPrefs.GetString("UserId");
        //}
        //else 
        //{
        //    uID = Randoms.RandomWord(16);
        //    PlayerPrefs.SetString("UserId", uID);
        //}
        //PhotonNetwork.AuthValues.UserId = uID;

        PhotonNetwork.JoinLobby();

    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("User has been disconneted! Reason : " + cause);
        //PhotonNetwork.ReconnectAndRejoin();
        AsyncOperation asyncOp;
        if (cause != DisconnectCause.DisconnectByClientLogic)
        {
            asyncOp = SceneManager.LoadSceneAsync(0);
            PlayerPrefs.SetString("HomeMessage", "Disconnected from server. " + cause.ToString());
            StartCoroutine(coroutine_WaitForScene(asyncOp.progress, cause));

        }
    }

    IEnumerator coroutine_WaitForScene(float loadingProgress, DisconnectCause cause)
    {
        while (loadingProgress < 0.99f)
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);
        //MenuManager.instance.ShowMessage("Disconnected from server. Reason : " + cause);
    }

    public bool CreateRoom(string rmName = "", string rmCode = "")
    {
        PlayerPrefs.SetInt("IsRejoin", 0);

        if (rmName == "" || string.IsNullOrEmpty(rmName))
            rmName = GetRoomName();

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = (byte)2;
        roomOptions.PlayerTtl = 1000;
        roomOptions.EmptyRoomTtl = 1000;

        tempHash[CP_ROOMCODE] = rmCode == "" ? Randoms.RandomWord(6) : rmCode;
        tempHash[CP_GAMEVERSION] = Application.version;

        roomOptions.CustomRoomProperties = tempHash;
        roomOptions.CustomRoomPropertiesForLobby = new string[3] { CP_ROOMCODE, CP_ISVISIBLE, CP_IMPOSTERS };

        return PhotonNetwork.CreateRoom(rmName, roomOptions, TypedLobby.Default);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("Successfully created room");
    }



    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.LogError("Failed to connect! Error Code : " + returnCode + ". \n" + message);
    }

    public bool JoinRoom(string rmName)
    {
        PlayerPrefs.SetInt("IsRejoin", 0);
        return PhotonNetwork.JoinRoom(rmName);
    }

    public override void OnJoinedRoom()
    {
        PlayerPrefs.SetInt("HasMatch", 1);
        Debug.Log("Successfully joined room");
        PhotonNetwork.LoadLevel(3);
        //SceneManager.LoadScene(3);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.LogError("Joining failed. Error : " + returnCode + ". " + message);
    }

    public override void OnLeftRoom()
    {
        PlayerPrefs.SetInt("HasMatch", 0);
    }


    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        this.totalRooms = roomList;
        //this.totalRooms = PhotonNetwork.roo
    }

    public List<RoomInfo> GetVisibleRooms()
    {
        List<RoomInfo> visibleRooms = new List<RoomInfo>();

        foreach (var room in totalRooms)
        {
            Debug.Log(room.CustomProperties["IsVisible"]);
            if (room.IsOpen && room.IsVisible && (bool)room.CustomProperties["IsVisible"] == true && room.PlayerCount < room.MaxPlayers)
            {
                visibleRooms.Add(room);

            }
        }
        Debug.Log(visibleRooms.Count);
        return visibleRooms;
    }

    string GetRoomName()
    {
        string roomName = PlayerPrefs.GetString("Name");
        bool isUnique = false;

        do
        {
            int x = 0;
            for (int i = 0; i < totalRooms.Count; i++)
            {
                if (totalRooms[i].Name == roomName)
                {
                    x++;
                    roomName = roomName + "Q";
                    break;
                }
            }

            if (x == 0)
            {
                isUnique = true;
            }
        }
        while (!isUnique);

        return roomName;

    }

}
