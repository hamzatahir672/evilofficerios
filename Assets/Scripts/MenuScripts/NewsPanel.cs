﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NewsPanel : MonoBehaviour
{
    [SerializeField] GameObject newsUI_Prefab;
    [SerializeField] Transform newUI_Parent;
    [SerializeField] Text errorTxt;
    [SerializeField] GameObject loadingObj;

    private List<NewsUI> newsBoxes = new List<NewsUI>();
    private NewsManager newsManager;

    private void Awake()
    {
        newsManager = FindObjectOfType<NewsManager>();
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        Debug.Log("Calledsds");
        ClearNews();
        loadingObj.SetActive(true);
        errorTxt.gameObject.SetActive(false);
        SetupNews();
    }

    private void SetupNews()
    {
        StartCoroutine(coroutine_SetupNews());
    }

    IEnumerator coroutine_SetupNews()
    {
        using (UnityWebRequest www = UnityWebRequest.Post(newsManager.NewsURL, ""))
        {
            yield return www.SendWebRequest();
            Debug.LogError(newsManager.NewsURL);
            if (www.isNetworkError)
            {
                loadingObj.SetActive(false);
                errorTxt.gameObject.SetActive(true);
                errorTxt.text = "Network Error. Please Check your Connection.";
            }
            else if (www.isHttpError) 
            {
                loadingObj.SetActive(false);
                errorTxt.gameObject.SetActive(true);
                errorTxt.text = "Server Error. Try Again Later";
            }
            else
            {
                //Form upload complete, text is the stuff that comes back.                
                string errorCheck = www.downloadHandler.text;
                Debug.LogError(errorCheck + "tom");
                if (errorCheck.Contains("Maintainence"))
                {
                    loadingObj.SetActive(false);
                    errorTxt.gameObject.SetActive(true);
                    errorTxt.text = "Maintainence Break.";
                }
                else
                {
                    try
                    {
                        JSONNode N = JSON.Parse(errorCheck);
                        //Debug.LogError("type is " + N["newsData"]["news"].GetType());
                        JSONArray newsArray = (JSONArray)N["newsData"]["news"];
                        string newsData = File.ReadAllText(newsManager.newsFilePath);

                        for (int i = 0; i < newsArray.Count; i++)
                        {
                            NewsUI newsDisplay = Instantiate(newsUI_Prefab, newUI_Parent).GetComponent<NewsUI>();

                            string heading = (JSONString)newsArray[i]["heading"];
                            string descr = (JSONString)newsArray[i]["description"];
                            string date = (JSONString)newsArray[i]["date"];
                            string url = (JSONString)newsArray[i]["url"];
                            string code = (JSONString)newsArray[i]["code"];

                            if (!newsData.Contains(code)) 
                            {
                                newsData += ", " + code;
                                File.WriteAllText(newsManager.newsFilePath, newsData);
                            }

                            newsDisplay.SetNewsInfo(heading, descr, date, url);
                            newsBoxes.Add(newsDisplay);
                        }
                        loadingObj.SetActive(false);
                        errorTxt.gameObject.SetActive(false);
                    }
                    catch (System.Exception e)
                    {
                        loadingObj.SetActive(false);
                        errorTxt.gameObject.SetActive(true);
                        Debug.LogError("Error: " + e + ". Reason: " + e.Message);
                        errorTxt.text = "Internal Error. Please Report Us.";
                    }

                    //Debug.LogError("News is " + heading + ". Array length is " + newsArray.Count.ToString());

                }
            }
        }
    }

    void ClearNews() 
    {
        foreach (var news in newsBoxes)
        {
            Destroy(news.gameObject);
        }

        newsBoxes.Clear();
    }
}
