﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkNameField : MonoBehaviour
{
    public static string currentName;

    [SerializeField] InputField nameField;
    [SerializeField] Text nameText;

    [SerializeField] GameObject namePanel;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey(PlayerPrefKeys.NAME_KEY))
            SetName(PlayerPrefs.GetString(PlayerPrefKeys.NAME_KEY));
        else 
            SetName(aitcHUtils.Randoms.RandomWord(4, false));
    }

    #region UI Functions
    public void onClick_SetName() 
    {
        if (nameField.text.Length < 3) 
        {
            UnityIAP.instance.ShowMessage("Name must contain 3 or more letters", 2f);
            return;
        }

        SetName(nameField.text);
        namePanel.SetActive(false);
    }
    #endregion

    #region Private Functions
    private void SetName(string _name) 
    {
        nameField.text = _name;
        currentName = _name;
        nameText.text = _name;
        PlayerPrefs.SetString(PlayerPrefKeys.NAME_KEY, _name);
    }
    #endregion
}
