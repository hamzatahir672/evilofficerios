﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewsUI : MonoBehaviour
{
    [SerializeField] Text headingTxt;
    [SerializeField] Text descriptionTxt;
    [SerializeField] Text dateTxt;

    private string heading;
    private string description;
    private string date;
    private string url = "";

    public void SetNewsInfo(string _heading, string _description, string _date, string _url)
    {
        this.heading = _heading;
        this.description = _description;
        this.date = _date;
        this.url = _url;

        headingTxt.text = _heading;
        descriptionTxt.text = _description;
        dateTxt.text = _date;
    }

    public void onClick_OpenUrl()
    {
        if (!string.IsNullOrEmpty(url) || this.url != "") 
        {
            Application.OpenURL(url);
        }
    }
}
