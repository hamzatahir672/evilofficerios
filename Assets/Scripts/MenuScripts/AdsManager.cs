﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AdsManager : MonoBehaviour
{
    #region Singleton
    //public static AdsManager instance;

    //void Awake()
    //{
    //    if(instance == null) instance = this;
    //    else if(instance != this) Destroy(gameObject);
    //}
    #endregion

    //private BannerView bannerView;
    //private InterstitialAd interstitial;
    //private RewardedAd rewardedAd;

    ////private string rewardId;

    //public bool ShowTestAds = false;
    //public GameObject testText;

    //public GameObject loadingAd;
    //public GameObject adFailed;
    //public GameObject adClosed;
    //public GameObject newDayDialog;
    //public playerCaught caught;

    //public string InterstitialId = "ca-app-pub-2905197017692408/2264495989";
    //public string RewardedId = "ca-app-pub-2905197017692408/3198303701";


    //// public Text coinText;

    //// Start is called before the first frame update
    //void Start()
    //{
    //    if (ShowTestAds) 
    //    {
    //        InterstitialId = "ca-app-pub-3940256099942544/1033173712";
    //        RewardedId = "ca-app-pub-3940256099942544/5224354917";
    //        if (testText != null) 
    //        {
    //            testText.SetActive(true);
    //            DontDestroyOnLoad(testText);
    //        }
    //    }

    //    // Initialize the Google Mobile Ads SDK.
    //    MobileAds.Initialize(initStatus => { });

    //    // //Banner Ad
    //    // RequestBanner();
    //    //Interstitial Ad
    //    RequestInterstitial();
    //    // Reward Ad
    //    RequestRewardedAd();
    //}
    ////private void Update()
    ////{
    ////    if (Input.GetKeyUp(KeyCode.X)) 
    ////    {
    ////        adFailed.SetActive(false);
    ////        adClosed.SetActive(false);
    ////        loadingAd.SetActive(false);
    ////        newDayDialog.SetActive(false);
    ////        caught.StartNewDay(6, true);
    ////    }
    ////}

    //public void RequestBanner()
    //{
    //    #if UNITY_ANDROID
    //        string adUnitId = "ca-app-pub-3940256099942544/6300978111";
    //    #elif UNITY_IPHONE
    //        string adUnitId = "ca-app-pub-3940256099942544/2934735716";
    //    #else
    //        string adUnitId = "unexpected_platform";
    //    #endif

    //    if (this.bannerView != null)
    //    {
    //        this.bannerView.Destroy();
    //    }

    //    // Create a 320x50 banner at the top of the screen.
    //    this.bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

    //    // Called when an ad request has successfully loaded.
    //    this.bannerView.OnAdLoaded += this.HandleOnAdLoadedBanner;
    //    // Called when an ad request failed to load.
    //    this.bannerView.OnAdFailedToLoad += this.HandleOnAdFailedToLoadBanner;
    //    // Called when an ad is clicked.
    //    this.bannerView.OnAdOpening += this.HandleOnAdOpenedBanner;
    //    // Called when the user returned from the app after an ad click.
    //    this.bannerView.OnAdClosed += this.HandleOnAdClosedBanner;
    //    // Called when the ad click caused the user to leave the application.
    //    this.bannerView.OnAdLeavingApplication += this.HandleOnAdLeavingApplicationBanner;

    //    // Create an empty ad request.
    //    AdRequest request = new AdRequest.Builder().Build();

    //    // Load the banner with the request.
    //    this.bannerView.LoadAd(request);
        
    //}

    //private void RequestInterstitial()
    //{
    //    #if UNITY_ANDROID
    //        string adUnitId = InterstitialId;
    //    #elif UNITY_IPHONE
    //        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
    //    #else
    //        string adUnitId = "unexpected_platform";
    //    #endif

    //    // Initialize an InterstitialAd.
    //    this.interstitial = new InterstitialAd(adUnitId);
    //    // Create an empty ad request.
    //    AdRequest request = new AdRequest.Builder().Build();
    //    // Load the interstitial with the request.
    //    this.interstitial.LoadAd(request);

    //    // Called when an ad request has successfully loaded.
    //    this.interstitial.OnAdLoaded += HandleOnAdLoaded;
    //    // Called when an ad request failed to load.
    //    this.interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
    //    // Called when an ad is shown.
    //    this.interstitial.OnAdOpening += HandleOnAdOpened;
    //    // Called when the ad is closed.
    //    this.interstitial.OnAdClosed += HandleOnAdClosed;
    //    // Called when the ad click caused the user to leave the application.
    //    this.interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplication;
    //}

    //private void RequestRewardedAd()
    //{
    //    this.rewardedAd = new RewardedAd(RewardedId);
    //    // Create an empty ad request.
    //    AdRequest request = new AdRequest.Builder().Build();
    //    // Load the rewarded ad with the request.
    //    this.rewardedAd.LoadAd(request);

    //    // Called when an ad request has successfully loaded.
    //    this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
    //    // Called when an ad request failed to load.
    //    this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
    //    // Called when an ad is shown.
    //    this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
    //    // Called when an ad request failed to show.
    //    this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
    //    // Called when the user should be rewarded for interacting with the ad.
    //    this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
    //    // Called when the ad is closed.
    //    this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;

    //}

    //public void ShowBannerAd()
    //{
    //    this.bannerView.Show();
    //}
    //public void ShowInterstitialAd()
    //{
    //    if (this.interstitial.IsLoaded()) {
    //        Debug.Log("Showing ad");
    //        this.interstitial.Show();
    //    }
    //}

    //public void ShowRewardedAd()
    //{
    //    //this.rewardId = rewardId;
    //    if (this.rewardedAd.IsLoaded())
    //    {
    //        this.rewardedAd.Show();
    //        StartCoroutine(checkingForAd());
    //    }
    //    else 
    //    {
    //        adFailed.SetActive(true);
    //    }
    //}

    //IEnumerator checkingForAd() 
    //{
    //    loadingAd.SetActive(true);
    //    yield return new WaitForSeconds(4f);
    //    if (loadingAd.activeInHierarchy) 
    //    {
    //        loadingAd.SetActive(false);
    //        adFailed.SetActive(true);
    //    }
    //}

    //#region RewardedAd
    //public void HandleRewardedAdLoaded(object sender, EventArgs args)
    //{
    //    MonoBehaviour.print("Ad loaded");

    //}

    //public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    //{
    //    MonoBehaviour.print(
    //        "Ad failed "
    //                         + args.Message);
    //    //adFailed.SetActive(true);
    //}

    //public void HandleRewardedAdOpening(object sender, EventArgs args)
    //{
    //    MonoBehaviour.print("opening ad");
    //}

    //public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    //{
    //    MonoBehaviour.print(
    //        "failed to show "
    //                         + args.Message);
    //    UnityMainThread.wkr.AddJob(() =>
    //    {
    //        adFailed.SetActive(true);
    //    });
    //}

    //public void HandleRewardedAdClosed(object sender, EventArgs args)
    //{
    //    this.RequestRewardedAd();
    //    UnityMainThread.wkr.AddJob(() =>
    //    {
    //        adClosed.SetActive(true);
    //    });
    //}

    //public void HandleUserEarnedReward(object sender, Reward args)
    //{
    //    Debug.Log("Reward Earned");

    //    //UnityMainThread.wkr.AddJob(() =>
    //    //{
    //    //    // Will run on main thread, hence issue is solved
    //    //    adFailed.SetActive(false);
    //    //    adClosed.SetActive(false);
    //    //    loadingAd.SetActive(false);
    //    //    newDayDialog.SetActive(false);
    //    //    caught.StartNewDay(6, true);
    //    //});
    //    UnityIAP.instance.ShowMessage("You got 60 Coins");
    //    loadingAd.SetActive(false);
    //    adFailed.SetActive(false);
    //    adClosed.SetActive(false);
    //    //newDayDialog.SetActive(false);
    //    MenuManager.instance.UpdateCoins(60);

    //    Debug.Log("Reward Given");

    //}
    //#endregion

    //#region InterstitialAd
    //public void HandleOnAdLoaded(object sender, EventArgs args)
    //{
    //    print("Ad Loaded!!!");
    //}

    //public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    //{
    //    MonoBehaviour.print("Ad Failed to Loaded!!!"
    //                        + args.Message);
    //}

    //public void HandleOnAdOpened(object sender, EventArgs args)
    //{
    //    MonoBehaviour.print("Ad Opened!!!!");
    //}

    //public void HandleOnAdClosed(object sender, EventArgs args)
    //{
    //    this.RequestInterstitial();
    //}

    //public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    //{
    //    MonoBehaviour.print("user exit the application");
    //}
    //#endregion


    //#region BannerAd
    //public void HandleOnAdLoadedBanner(object sender, EventArgs args)
    //{
    //    MonoBehaviour.print("HandleAdLoaded event received");
    //}

    //public void HandleOnAdFailedToLoadBanner(object sender, AdFailedToLoadEventArgs args)
    //{
    //    MonoBehaviour.print("HandleFailedToReceiveAd event received with message: "
    //                        + args.Message);
    //    Invoke("RequestBanner", 3f);              
    //}

    //public void HandleOnAdOpenedBanner(object sender, EventArgs args)
    //{
    //    MonoBehaviour.print("HandleAdOpened event received");
    //}

    //public void HandleOnAdClosedBanner(object sender, EventArgs args)
    //{
    //    Invoke("RequestBanner", 3f);
    //}

    //public void HandleOnAdLeavingApplicationBanner(object sender, EventArgs args)
    //{
    //    MonoBehaviour.print("HandleAdLeavingApplication event received");
    //}
    //#endregion
}
