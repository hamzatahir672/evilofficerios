﻿using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class RoomListing : MonoBehaviourPunCallbacks
{
    public Text roomNameText;
    //public Text roomPlayerText;
    //public Text roomImposterText;
    public RoomInfo roomInfo;

    public void SetRoomInfo(RoomInfo info)
    {
        roomNameText.text = info.Name;
        //roomPlayerText.text = info.PlayerCount + "/" + info.MaxPlayers;
        //roomImposterText.text = ((byte)info.CustomProperties[NetworkController.instance.CP_IMPOSTERS]).ToString();
        roomInfo = info;
    }

    public void OnClick_JoinRoom()
    {
        NetworkController.instance.JoinRoom(roomInfo.Name);
    }


}
