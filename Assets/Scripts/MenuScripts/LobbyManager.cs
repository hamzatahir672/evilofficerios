﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    public Transform listingParent;
    public RoomListing roomListing_prefab;

    private List<RoomListing> listings = new List<RoomListing>();

    [SerializeField] int maxListings = 8;
    [SerializeField] GameObject[] lobbyTabs;
    [SerializeField] GameObject[] lobbyPanels;
    [SerializeField] FadeTransition blackTransition;


    private void Start()
    {
        blackTransition.FadeOut();
        UpdateRoomList();
    }
    public void UpdateRoomList()
    {
        Debug.Log("Updating Lists");
        List<RoomInfo> roomList = NetworkController.instance.GetVisibleRooms();

        int i = 0;

        foreach (var listing in listings)
        {
            Destroy(listing.gameObject);
        }
        listings.Clear();

        foreach (var roomInfo in roomList)
        {
            if (i >= maxListings)
                return;

            RoomListing obj = Instantiate(roomListing_prefab, listingParent);
            Debug.Log("Adding to List");

            if (obj != null)
            {
                obj.SetRoomInfo(roomInfo);
                listings.Add(obj);
            }
            i++;
        }
    }

    public void SetTab(int tabSelected)
    {
        for (int i = 0; i < lobbyTabs.Length; i++)
        {
            if (i == tabSelected)
            {
                lobbyTabs[i].transform.localScale = new Vector3(1.3f, 1.3f, 1);
                lobbyPanels[i].SetActive(true);
            }
            else
            {
                lobbyTabs[i].transform.localScale = new Vector3(1f, 1f, 1);
                lobbyPanels[i].SetActive(false);
            }
        }
    }

    public void ReturnHome() 
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
