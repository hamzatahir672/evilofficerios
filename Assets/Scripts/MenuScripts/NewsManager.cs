﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;
using SimpleJSON;

public class NewsManager : MonoBehaviour
{
    [SerializeField] string _newsUrl;
    [SerializeField] GameObject newNewsIcon;

    /// <summary>
    /// The Json file where news already viewed stored. /newsDat.json
    /// </summary>
    public string newsFilePath { get { return Application.persistentDataPath + "/newsDat.json"; } }
    public string NewsURL { get { return _newsUrl; } }

    // Start is called before the first frame update
    void Start()
    {
        if (!File.Exists(newsFilePath))
            File.WriteAllText(newsFilePath, "");

        InvokeRepeating("CheckNews", 1f, 3f);
    }

    void CheckNews() 
    {
        StartCoroutine(coroutine_CheckNews());
    }

    IEnumerator coroutine_CheckNews()
    {
        using (UnityWebRequest www = UnityWebRequest.Post(NewsURL, ""))
        {
            yield return www.SendWebRequest();
            //Debug.LogError(NewsURL);
            if (!www.isNetworkError && !www.isHttpError)
            {
                //Form upload complete, text is the stuff that comes back.                
                string errorCheck = www.downloadHandler.text;

                try
                {
                    JSONNode N = JSON.Parse(errorCheck);
                    //Debug.LogError("type is " + N["newsData"]["news"].GetType());
                    JSONArray newsArray = (JSONArray)N["newsData"]["news"];
                    string newsData = File.ReadAllText(newsFilePath);

                    for (int i = 0; i < newsArray.Count; i++)
                    {
                        string code = (JSONString)newsArray[i]["code"];

                        if (!newsData.Contains(code))
                        {
                            // New News available
                            newNewsIcon.SetActive(true);
                            break;
                        }
                        else 
                        {
                            newNewsIcon.SetActive(false) ;
                        }
                    }
                }
                catch (System.Exception)
                {

                }
            }

        }

        yield return null;
    }
}
