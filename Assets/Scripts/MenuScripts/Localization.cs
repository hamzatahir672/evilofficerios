﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

public class Localization : MonoBehaviour
{
    public static Localization instance;
    public static JSONNode LanguageJSON { get { return JSONNode.Parse(jsonString); } }

    static string jsonString = "";
    public LanguageAsset langAsset;
    TextLocalization[] localizedItems;

    [SerializeField] string webURL;
    
    void Awake()
    {
        instance = this;
        if (jsonString == "") 
        {
            StartCoroutine(LoadLocalizedText((status) =>
            {
                if (!status)
                {
                    //Failed
                    UnityIAP.instance.ShowMessage("Failed to load languages!! Try restarting game");
                }
        
            }));
        }
        localizedItems = Resources.FindObjectsOfTypeAll(typeof( TextLocalization)) as TextLocalization[];

    }

    private void Start()
    {
        //UpdateLanguage();
    }

    public void UpdateLanguage() 
    {
        //Debug.Log(PlayerPrefs.GetInt("LanguageIndex"));
        foreach (var item in localizedItems)
        {
            item.UpdateText(langAsset.Languages[PlayerPrefs.GetInt("LanguageIndex")].languageFont);
        }
    }

    IEnumerator GetLanguageData() 
    {
        UnityWebRequest www = UnityWebRequest.Get(webURL);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            UnityIAP.instance.ShowMessage("Error retriving language data!! Please check you connection and restart the game.");
            jsonString = "";
        }
        else 
        {
            jsonString = www.downloadHandler.text;
            Debug.Log("Success");
            UpdateLanguage();
        }
    }

    public IEnumerator LoadLocalizedText(Action<bool> success)
    {
        //localizedText = new Dictionary<string, string>();
        string filePath = Path.Combine(Application.streamingAssetsPath, "EO_Language.json");

        string dataAsJson = "";

        //Android
        if (filePath.Contains("://"))
        {

            WWW reader = new WWW(filePath);

            //Wait(Non blocking until download is done)
            while (!reader.isDone)
            {
                yield return null;
            }

            if (reader.text == null || reader.text == "")
            {
                success(false);

                //Just like return false
                yield break;
            }

            dataAsJson = reader.text;
        }

        //iOS
        else
        {
            dataAsJson = System.IO.File.ReadAllText(filePath);
        }


        jsonString = dataAsJson;

        if (dataAsJson == "")
            success(false);
        else
        {
            success(true);
        }
    }
}
