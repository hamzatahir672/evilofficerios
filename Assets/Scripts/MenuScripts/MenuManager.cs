﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Rendering;
using System.IO;
using Photon.Pun;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;

    public enum MenuType { Main, Lobby }
    public MenuType menuType;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public GameObject page01;
    public GameObject page02;
    public GameObject page03;
    public Dropdown difficultyDD;
    public Slider AudioSlider;
    public Slider SensitivitySlider;
    public string gameModeName;
    public string difficultyName;
    public int gameMode;
    public int difficultyLevel;
    public GameObject startButton;
    public GameObject shopPanel;
    public GameObject facebookLike;
    public GameObject youtubeSubscribe;
    public AdsManager adManager;
    public CameraPPController cameraObj;

    [SerializeField] FadeTransition blackTransition;

    [Header("Coins")]
    public static int CoinsCount = 0;
    [SerializeField] Text coinText;
    [SerializeField] int OneLevelCost = 1000;
    [SerializeField] int AllLevelCost = 3000;

    Animator animator;
    CanvasGroup selectedPage = null;

    [SerializeField] GameObject guidePanel;
    [SerializeField] CanvasGroup[] pages;
    [SerializeField] GameObject[] gameModeBtns;
    [SerializeField] GameObject[] difficultyBtns;
    
    [SerializeField] GameObject[] loadingHelps;
    [SerializeField] GameObject loadingScreen;
    [SerializeField] GameObject removeAdsBtn;
    [SerializeField] GameObject noInteractionObj;

    [Header("Settings")]

    [SerializeField] GameObject[] languageBtns;
    [SerializeField] GameObject[] settingTabs;
    [SerializeField] GameObject[] settingPanels;
    //[SerializeField] GameObject gameTab;
    //[SerializeField] GameObject languageTab;
    [SerializeField] Text graphicText;
    [SerializeField] Text textureText;
    [SerializeField] RenderPipelineAsset[] pipelines;
    [SerializeField] InputField nameField;
    int qualityIndex = 3;
    int textureIndex = 0;

    [Header("How to Play")]
    [SerializeField] GameObject[] hintPages; 
    [SerializeField] GameObject hintBtnPrevious; 
    [SerializeField] GameObject hintBtnNext; 
    [SerializeField] AudioManager soundManager;
    int hintPageIndex = 0;

    [Header("Dialogs")]
    [SerializeField] GameObject buyDifficultyDialog;
    [SerializeField] GameObject buyRemoveAdsDialog;

    Localization localizationController;

    delegate void QualityChange(bool state);
    QualityChange OnQualityChange;
    private GameObject activeLoadingIndicator;
    private float loadingTimeout = 5;

    // Start is called before the first frame update
    void Start()
    {
        blackTransition.FadeOut();
        PlayerPrefs.SetInt("N1", 1);
        PlayerPrefs.SetInt("N2", 1);
        PlayerPrefs.SetInt("N3", 1);
        PlayerPrefs.SetInt("N4", 1);
        PlayerPrefs.SetString("NoAds", "1");

        if (PlayerPrefs.GetInt("ytSubscribe", 0) == 1) 
        {
            youtubeSubscribe.SetActive(false);
        }

        if (PlayerPrefs.GetInt("fbLike", 0) == 1) 
        {
            facebookLike.SetActive(false);
        }

        //if (adManager.ShowTestAds)
        //{
        //    if (PlayerPrefs.GetInt("ZomboCoins", 30) < 50) 
        //    {
        //        UpdateCoins(4500);
        //    }
        //}
        //else 
        //{
        //    CoinsCount = PlayerPrefs.GetInt("ZomboCoins", 30);
        //}

        qualityIndex = PlayerPrefs.GetInt("Quality", 0);
        QualitySettings.SetQualityLevel(qualityIndex) ;

        textureIndex = PlayerPrefs.GetInt("TextureIndex", 0);
        textureText.text = (textureIndex + 1).ToString();
        OnQualityChange = cameraObj.SetCameraPP;

        SetQuality();
        UpdateCoins();


        //if (PlayerPrefs.GetString("NoAds", "0") == "1") 
        //{
        //    removeAdsBtn.SetActive(false);
        //}

        if (PlayerPrefs.GetInt("Guides", 0) == 1) 
        {
            //Debug.LogError("Hided Guides");
            guidePanel.SetActive(false);
        }
        localizationController = Localization.instance;
        PlayerPrefs.GetString("Difficulty", "Normal");
        animator = GetComponent<Animator>();
        SetHintPages();


        switch (Application.systemLanguage) 
        {
            case SystemLanguage.French :
                ChangeGameLanguage(PlayerPrefs.GetString("Language", "FR"));
                SetLanguage(PlayerPrefs.GetInt("LanguageIndex", 1));
                break;
            case SystemLanguage.Italian:
                ChangeGameLanguage(PlayerPrefs.GetString("Language", "IT"));
                SetLanguage(PlayerPrefs.GetInt("LanguageIndex", 5));
                break;
            case SystemLanguage.Spanish:
                ChangeGameLanguage(PlayerPrefs.GetString("Language", "SP"));
                SetLanguage(PlayerPrefs.GetInt("LanguageIndex", 4));
                break;
            case SystemLanguage.Portuguese:
                ChangeGameLanguage(PlayerPrefs.GetString("Language", "PT"));
                SetLanguage(PlayerPrefs.GetInt("LanguageIndex", 7));
                break;
            case SystemLanguage.Russian:
                ChangeGameLanguage(PlayerPrefs.GetString("Language", "RU"));
                SetLanguage(PlayerPrefs.GetInt("LanguageIndex", 8));
                break;
            case SystemLanguage.Turkish:
                ChangeGameLanguage(PlayerPrefs.GetString("Language", "TR"));
                SetLanguage(PlayerPrefs.GetInt("LanguageIndex", 9));
                break;
            default:
                ChangeGameLanguage(PlayerPrefs.GetString("Language", "EN"));
                SetLanguage(PlayerPrefs.GetInt("LanguageIndex", 0));
                break;
        }
        
        AudioSlider.value = PlayerPrefs.GetFloat("AudioVolume", 0.5f);
        
    }

    public void GuideShowed() 
    {
        PlayerPrefs.SetInt("Guides", 1);
    }
    
    public void SelectPageWithTransition(int index)
    {

        foreach(CanvasGroup page in pages)
        {
            page.interactable = false;
        }

        StartCoroutine(MenuSelect(index, 2f, 0));
    }

    public void SelectPageWithoutTransition(int index)
    {
        

        foreach(CanvasGroup page in pages)
        {
            page.interactable = false;
        }

        if (pages[1].gameObject.activeInHierarchy)
        {
            StartCoroutine(MenuSelect(index, 2f, 1, 1));
        }
        else if (pages[5].gameObject.activeInHierarchy)
        {
            StartCoroutine(MenuSelect(index, 2f, 1, 5));
        }
        else 
        {
            StartCoroutine(MenuSelect(index, 2f, 1));
        }
    }

    public void SetGameMode(int gameModeSelected)
    {
        gameMode = gameModeSelected;

        if (gameMode == 0)
        {
            gameModeName = "Classic";
            PlayerPrefs.SetString("GameMode", gameModeName);
        }
        else if (gameMode == 1)
        {
            gameModeName = "Nightmare";
            PlayerPrefs.SetString("GameMode", gameModeName);
        }
    }

    public void SetDifficulty(int difficultySelected)
    {
        if (PlayerPrefs.GetString("GameMode") == "Nightmare") 
        {
            if (difficultySelected == 1) 
            {
                if (PlayerPrefs.GetInt("N1", 0) != 1) 
                {
                    buyDifficultyDialog.SetActive(true);
                    buyDifficultyDialog.name = "N1";
                    return;
                }
            }

            if (difficultySelected == 2)
            {
                if (PlayerPrefs.GetInt("N2", 0) != 1)
                {
                    buyDifficultyDialog.SetActive(true);
                    buyDifficultyDialog.name = "N2";
                    return;
                }
            }

            if (difficultySelected == 3)
            {
                if (PlayerPrefs.GetInt("N3", 0) != 1)
                {
                    buyDifficultyDialog.SetActive(true);
                    buyDifficultyDialog.name = "N3";
                    return;
                }
            }

            if (difficultySelected == 4)
            {
                if (PlayerPrefs.GetInt("N4", 0) != 1)
                {
                    buyDifficultyDialog.SetActive(true);
                    buyDifficultyDialog.name = "N4";
                    return;
                }
            }
        }


        difficultyLevel = difficultySelected;
        startButton.SetActive(true);

        for(int i = 0; i < difficultyBtns.Length; i++)
        {
            if(i == difficultySelected)
            {
                difficultyBtns[i].transform.localScale = new Vector3(1.3f, 1.3f, 1);
            }
            else
            {
                difficultyBtns[i].transform.localScale = new Vector3(1f, 1f, 1);
            }
        }

        if(difficultyLevel == 5)
        {
            difficultyName = "Ghost";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        } 
        else if(difficultyLevel == 4)
        {
            difficultyName = "Extreme";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if(difficultyLevel == 3)
        {
            difficultyName = "Hard";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if(difficultyLevel == 2)
        {
            difficultyName = "Normal";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if(difficultyLevel == 1)
        {
            difficultyName = "Easy";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if(difficultyLevel == 0)
        {
            difficultyName = "Practice";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
    }

    public void TryDifficulty() 
    {
        if (CoinsCount >= 100)
        {
            UpdateCoins(-100);
            buyDifficultyDialog.SetActive(false);
        }
        else 
        {
            Debug.Log("Not Enough Coins");
            shopPanel.SetActive(true);
            buyDifficultyDialog.SetActive(false);
            return;
        }

        if (buyDifficultyDialog.name == "N1")
        {
            difficultyLevel = 1;
        }
        else if (buyDifficultyDialog.name == "N2")
        {
            difficultyLevel = 2;
        }
        else if (buyDifficultyDialog.name == "N3")
        {
            difficultyLevel = 3;
        }
        else if (buyDifficultyDialog.name == "N4")
        {
            difficultyLevel = 4;
        }

        startButton.SetActive(true);

        for (int i = 0; i < difficultyBtns.Length; i++)
        {
            if (i == difficultyLevel)
            {
                difficultyBtns[i].transform.localScale = new Vector3(1.3f, 1.3f, 1);
            }
            else
            {
                difficultyBtns[i].transform.localScale = new Vector3(1f, 1f, 1);
            }
        }

        if (difficultyLevel == 5)
        {
            difficultyName = "Nightmare";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if (difficultyLevel == 4)
        {
            difficultyName = "Extreme";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if (difficultyLevel == 3)
        {
            difficultyName = "Hard";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if (difficultyLevel == 2)
        {
            difficultyName = "Normal";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if (difficultyLevel == 1)
        {
            difficultyName = "Easy";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if (difficultyLevel == 0)
        {
            difficultyName = "Practice";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
    }

    public void BuyRemoveAds() 
    {
        if (CoinsCount >= 1500) 
        {
            UpdateCoins(-1500);
            PlayerPrefs.SetString("NoAds", "1");
            removeAdsBtn.SetActive(false);
            buyRemoveAdsDialog.SetActive(false);
        }
        else
        {
            Debug.Log("Not Enough Coins");
            shopPanel.SetActive(true);
            buyRemoveAdsDialog.SetActive(false);
        }
    }

    public void BuyDifficulty(int all = 0) // 0 for one, 1 for all 
    {
        if (CoinsCount >= AllLevelCost)
        {
            if (all == 1) 
            {
                PlayerPrefs.SetInt("N1", 1);
                PlayerPrefs.SetInt("N2", 1);
                PlayerPrefs.SetInt("N3", 1);
                PlayerPrefs.SetInt("N4", 1);
                UpdateCoins(-AllLevelCost);
                buyDifficultyDialog.SetActive(false);
                return;
            }
        }
        else
        {
            if (all == 1)
            {
                Debug.Log("Not Enough Coins");
                shopPanel.SetActive(true);
                buyDifficultyDialog.SetActive(false);
            }
        }

        if (CoinsCount >= OneLevelCost)
        {

            if (buyDifficultyDialog.name == "N1")
            {
                PlayerPrefs.SetInt("N1", 1);
                UpdateCoins(-OneLevelCost);
            }
            else if (buyDifficultyDialog.name == "N2")
            {
                PlayerPrefs.SetInt("N2", 1);
                UpdateCoins(-OneLevelCost);
            }
            else if (buyDifficultyDialog.name == "N3")
            {
                PlayerPrefs.SetInt("N3", 1);
                UpdateCoins(-OneLevelCost);
            }
            else if (buyDifficultyDialog.name == "N4")
            {
                PlayerPrefs.SetInt("N4", 1);
                UpdateCoins(-OneLevelCost);
            }
            buyDifficultyDialog.SetActive(false);
        }
        else 
        {
            Debug.Log("Not Enough Coins");
            shopPanel.SetActive(true);
            buyDifficultyDialog.SetActive(false);
        }
    }

    public void SetSettingsTab(int tabSelected)
    {
        for(int i = 0; i < settingTabs.Length; i++)
        {
            if(i == tabSelected)
            {
                settingTabs[i].transform.localScale = new Vector3(1.3f, 1.3f, 1);
                settingPanels[i].SetActive(true);
            }
            else
            {
                settingTabs[i].transform.localScale = new Vector3(1f, 1f, 1);
                settingPanels[i].SetActive(false);
            }
        }

        
        //if(tabSelected == 0)
        //{
        //    gameTab.SetActive(true);
        //    languageTab.SetActive(false);
        //}
        //else if(tabSelected == 1)
        //{
        //    gameTab.SetActive(false);
        //    languageTab.SetActive(true);
        //}
    }

    public void SetLanguage(int languageSelected)
    {
        PlayerPrefs.SetInt("LanguageIndex", languageSelected);


        for (int i = 0; i < languageBtns.Length; i++)
        {
            if(i == languageSelected)
            {
                languageBtns[i].transform.localScale = new Vector3(1.3f, 1.3f, 1);
            }
            else
            {
                languageBtns[i].transform.localScale = new Vector3(1f, 1f, 1);
            }
        }
    }

    public void AdjustGraphic(int changeValue) 
    {
        Debug.Log("Graphic Changed");
        qualityIndex += changeValue;


        if (qualityIndex < 0) 
        {
            qualityIndex = 4;
        }
        if (qualityIndex > 4) 
        {
            qualityIndex = 0;
        }

        QualitySettings.SetQualityLevel(qualityIndex);

        PlayerPrefs.SetInt("Quality", QualitySettings.GetQualityLevel());

        SetQuality();   
    }

    public void AdjustTexture(int changeValue)
    {
        textureIndex += changeValue;

        if (textureIndex < 0)
        {
            textureIndex = 1;
        }
        if (textureIndex > 1)
        {
            textureIndex = 0;
        }

        PlayerPrefs.SetInt("TextureIndex", textureIndex);

        textureText.text = (textureIndex + 1).ToString();
    }

    void SetQuality() 
    {
        switch (QualitySettings.GetQualityLevel())
        {
        case 0:
                graphicText.text = "Very Low";
                GraphicsSettings.renderPipelineAsset = pipelines[0];
                Application.targetFrameRate = 30;
                OnQualityChange(false);
                break;

        case 1:
                graphicText.text = "Low";
                GraphicsSettings.renderPipelineAsset = pipelines[1];
                Application.targetFrameRate = 30;
                OnQualityChange(false);
                break;

        case 2:
                graphicText.text = "Medium";
                GraphicsSettings.renderPipelineAsset = pipelines[2];
                OnQualityChange(true);
                break;

        case 3:
                graphicText.text = "High";
                GraphicsSettings.renderPipelineAsset = pipelines[3];
                OnQualityChange(true);
                break;

        case 4:
                graphicText.text = "Ultra";
                GraphicsSettings.renderPipelineAsset = pipelines[4];
                OnQualityChange(true);
                break;
        }
    }



    public void AdjustVolume()
    {
        soundManager.AudioVolume = AudioSlider.value;
        soundManager.ChangeVolume();
    }

    public void AdjustSensitivity()
    {
        PlayerPrefs.SetFloat("Sensitivity", SensitivitySlider.value);
    }

    public void ChangeGameLanguage(string language)
    {
        PlayerPrefs.SetString("Language", language);
        //int languageIndex = 0;
        //TextLocalization[] localizedObjects = GameObject.FindObjectsOfType<TextLocalization>();

        //if(language == "EN")
        //{
        //    languageIndex = 0;
        //}
        //else if(language == "FR")
        //{
        //    languageIndex = 1;
        //}
        //else if(language == "UR")
        //{
        //    languageIndex = 2;
        //}
        //else if(language == "HN")
        //{
        //     languageIndex = 3;
        //}
        //else if (language == "SP")
        //{
        //    languageIndex = 4;
        //}
        //else if (language == "IT")
        //{
        //    languageIndex = 5;
        //}


        Localization.instance.UpdateLanguage();

        //foreach (TextLocalization obj in localizationController.localizedItems)
        //{   
        //    //obj.gameObject.GetComponent<Text>().text = obj.TextValues[languageIndex];
        //}
    }

    public void Page03()
    {
        page01.SetActive(false);
        page02.SetActive(false);
        page03.SetActive(true);

        difficultyLevel = difficultyDD.value;
        if(difficultyLevel == 0)
        {
            difficultyName = "Nightmare";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        } 
        else if(difficultyLevel == 1)
        {
            difficultyName = "Extreme";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if(difficultyLevel == 2)
        {
            difficultyName = "Hard";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if(difficultyLevel == 3)
        {
            difficultyName = "Normal";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if(difficultyLevel == 4)
        {
            difficultyName = "Easy";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }
        else if(difficultyLevel == 5)
        {
            difficultyName = "Practice";
            PlayerPrefs.SetString("Difficulty", difficultyName);
        }


    }

    public void Back(int transition = 0)
    {
        foreach(CanvasGroup page in pages)
        {
            page.interactable = false;
        }
        
        StartCoroutine(BacktoHome(2f, transition));
    }

    public void StartGame()
    {
        loadingScreen.SetActive(true);
        loadingHelps[Random.Range(0, loadingHelps.Length - 1)].SetActive(true);
        
        
        SceneManager.LoadScene(gameMode + 1);
    }

    IEnumerator MenuSelect(int pageIndex , float hideSpeed, int transitionType = 0, int currentPage = 0)
    {

        while(pages[currentPage].alpha > 0)
        {
            pages[currentPage].alpha -= Time.deltaTime * hideSpeed;
            yield return null;
        }

        if(transitionType == 0)
        {
            if(pages[0] .alpha <= 0)
            {
                animator.Play("start");
            }

            yield return new WaitForSeconds(2f);
        }

        

        pages[pageIndex].alpha = 0;
        pages[pageIndex].gameObject.SetActive(true);

        while(pages[pageIndex].alpha < 1)
        {
            pages[pageIndex].alpha += Time.deltaTime ;
            yield return null;
        }
        selectedPage = pages[pageIndex];

        if(selectedPage.alpha == 1)
        {
            selectedPage.interactable = true;
        }

        for(int i = 0; i < pages.Length; i++)
        {
            if(i == pageIndex)
            {
                pages[i].gameObject.SetActive(true);
            }
            else
            {
                pages[i].gameObject.SetActive(false);
            }
        }
    }

    IEnumerator BacktoHome(float hideSpeed, int transitionType = 0)
    {
        while(selectedPage.alpha > 0)
        {
            selectedPage.alpha -= Time.deltaTime * hideSpeed;
            yield return null;
        }

        if(transitionType == 0)
        {
            if(selectedPage.alpha <= 0)
            {
                animator.Play("back");
            }

            yield return new WaitForSeconds(2f);
        }


        pages[0].alpha = 0;
        pages[0].gameObject.SetActive(true);

        while(pages[0].alpha < 1)
        {
            pages[0].alpha += Time.deltaTime;
            yield return null;
        }

        if(pages[0].alpha == 1)
        {
            pages[0] .interactable = true;
        }
        selectedPage = null;

        for(int i = 0; i < pages.Length; i++)
        {
            if(i == 0)
            {
                pages[i].gameObject.SetActive(true);
            }
            else
            {
                pages[i].gameObject.SetActive(false);
            }
        }
        
    }

    public void NextHintPage()
    {
        if(hintPageIndex < hintPages.Length - 1)
        {
            hintPageIndex++;
            
            SetHintPages();
        }
    }

    public void PreviousHintPage()
    {
        if(hintPageIndex > 0)
        {
            hintPageIndex--;

            SetHintPages();
        }
    }

    void SetHintPages()
    {
        for(int i = 0; i < hintPages.Length; i++)
        {
            if(i == hintPageIndex)
            {
                hintPages[i].SetActive(true);
            }
            else
            {
                hintPages[i].SetActive(false);
            }
        }

        if(hintPageIndex == 0)
        {
            hintBtnPrevious.SetActive(false);
        }
        else
        {
            hintBtnPrevious.SetActive(true);
        }

        if(hintPageIndex == hintPages.Length - 1)
        {
            hintBtnNext.SetActive(false);
        }
        else
        {
            hintBtnNext.SetActive(true);
        }

    }

    public void TakeToYoutube() 
    {
        if (Application.internetReachability == NetworkReachability.NotReachable) 
        {
            UnityIAP.instance.ShowMessage("Please check your internet");
            return;
        }
        Application.OpenURL("https://www.youtube.com/user/hamzatahirful");
        StartCoroutine(checkCompletion("yt"));

    }

    public void TakeToFacebook() 
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            UnityIAP.instance.ShowMessage("Please check your internet");
            return;
        }
        Application.OpenURL("https://www.facebook.com/evilofficer");
        StartCoroutine(checkCompletion("fb"));
    }

    IEnumerator checkCompletion(string platform) 
    {
        yield return new WaitForSeconds(2f);

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            UnityIAP.instance.ShowMessage("Please check your internet and try again");
        }
        else 
        {
            UnityIAP.instance.ShowMessage("Thank you! You got 100 coins");
            UpdateCoins(100);
            if (platform == "yt") 
            {
                PlayerPrefs.SetInt("ytSubscribe", 1);
                youtubeSubscribe.SetActive(false);
            }
            else if (platform == "fb")
            {
                PlayerPrefs.SetInt("fbLike", 1);
                facebookLike.SetActive(false);
            }
        }
    }

    public void UpdateCoins(int amount = 0) 
    {
        //CoinsCount += amount;
        //coinText.text = CoinsCount.ToString();
        //PlayerPrefs.SetInt("ZomboCoins", CoinsCount);
    }

    public void onClick_LoadRoomsScene(GameObject indicator)
    {
        //if (nameField.text.Length < 3)
        //{
        //    nameField.GetComponent<Animator>().SetTrigger("shake");
        //    return;
        //}
        if (!PhotonNetwork.IsConnectedAndReady || !PhotonNetwork.InLobby)
        {

            activeLoadingIndicator = indicator;

            WaitForConnection(() =>
            {
                blackTransition.FadeIn(() =>
                {
                    SceneManager.LoadScene(1);
                });
            });
        }
        else 
        {
            blackTransition.FadeIn(() =>
            {
                SceneManager.LoadScene(1);
            });
        }

    }

    void WaitForConnection(System.Action function)
    {
        activeLoadingIndicator.SetActive(true);
        noInteractionObj.SetActive(true);
        StartCoroutine(coroutine_Loading(function));
    }

    IEnumerator coroutine_Loading(System.Action function)
    {
        float interval = 0;

        if (!PhotonNetwork.InLobby)
            PhotonNetwork.JoinLobby();

        while (!PhotonNetwork.IsConnectedAndReady || !PhotonNetwork.InLobby)
        {
            if (interval >= loadingTimeout)
            {
                activeLoadingIndicator.SetActive(false);
                UnityIAP.instance.ShowMessage("Connection Time Out. Please check your connection or try again in a few moments");
                noInteractionObj.SetActive(false);
                NetworkController.instance.ReconnectToServer();
                break;
            }

            interval += Time.deltaTime;
            yield return null;
        }
        //yield return new WaitForSeconds(loadingTimeout);
        if (interval < loadingTimeout)
        {
            function.Invoke();
        }
    }
}

public class PlayerPrefKeys 
{
    public static string NAME_KEY = "1";
}
