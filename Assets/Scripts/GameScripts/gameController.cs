﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameController : MonoBehaviour
{

    public static gameController instance; 
    public GameObject pauseMenu;
    public Text hintText; 
    public Text keypadInfoText;
    public Text keypadTypedText;
    public DisplayMessageController messageController;
    public FirstPersonController playerMovementController;
    public Slider SensitivitySlider;
    public codelockControl codeLock;

    void Awake()
    {
        instance = this;
        messageController = GetComponent<DisplayMessageController>();
    }

    private void Start()
    {
        playerMovementController.mouseLook.XSensitivity = PlayerPrefs.GetFloat("Sensitivity", 0.3f);
        playerMovementController.mouseLook.YSensitivity = PlayerPrefs.GetFloat("Sensitivity", 0.3f);

        SensitivitySlider.value = PlayerPrefs.GetFloat("Sensitivity", 0.3f);

        Debug.Log("Sensititvity Set to : " + PlayerPrefs.GetFloat("Sensitivity", 0.3f).ToString());
    }

    public void ChangeSensitivity() 
    {
        PlayerPrefs.SetFloat("Sensitivity", SensitivitySlider.value);
        playerMovementController.mouseLook.XSensitivity = PlayerPrefs.GetFloat("Sensitivity");
        playerMovementController.mouseLook.YSensitivity = PlayerPrefs.GetFloat("Sensitivity");
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadSceneAsync(0);
    }

    public void DisplayMessage(string _message, float _timer = 2f)
    {
        StartCoroutine(DisplayMessageCourotine(_message,_timer));
        Debug.Log("function called");
    } 

     IEnumerator DisplayMessageCourotine(string message, float timer)
    {
        Debug.Log(message);
        hintText.text = message;
        hintText.gameObject.SetActive(true);
        yield return  new WaitForSeconds(timer);
        hintText.gameObject.SetActive(false);
    }
}
