 using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;


[RequireComponent(typeof(CharacterController))]

public class FirstPersonController : MonoBehaviour
{
    public bool useKeyboard;
    public float forwardSpeed;
    public float backwardSpeed;
    public float sideSpeed;
    public Vector2 RunAxis;
    [SerializeField] private bool m_IsWalking;
    [SerializeField] private EnemyAI enemy;
    [SerializeField] private scorpionAI scorpion;
    [SerializeField] private float m_RunSpeed;
    // [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
    [SerializeField] private float m_JumpSpeed;
    [SerializeField] private float m_StickToGroundForce;
    [SerializeField] private float m_GravityMultiplier;
    [SerializeField] public MouseLook mouseLook;
    [SerializeField] private bool m_UseFovKick;
    [SerializeField] private FOVKick m_FovKick = new FOVKick();
    [SerializeField] private bool m_UseHeadBob;
    [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
    [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
    [SerializeField] private float m_StepInterval;
    [SerializeField] private Image m_StaminaMeter;
    [SerializeField] private StaminaController m_StaminaController;
    [SerializeField] private GameObject staminaIcon;
    [SerializeField] private GameObject staminaWarning;
    // an array of footstep sounds that will be randomly selected from.
    // [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
    // [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

    private Camera m_Camera;
    private bool m_Jump;
    private float m_YRotation;
    private Vector2 m_Input;
    private Vector3 m_MoveDir = Vector3.zero;
    private float m_StaminaLeft = 1f;
    private float m_StaminaDrainTime = 7f;
    private float m_StaminaRechargePerCycle = 10f;
    private float m_TotalStamina = 100f;
    private bool m_IsRunning = false;
    private bool m_IsExausted = false;
    private bool m_IsResting = false;
    private bool m_CanMove = true;
    private CharacterController m_CharacterController;
    private CollisionFlags m_CollisionFlags;
    private bool m_PreviouslyGrounded;
    private Vector3 m_OriginalCameraPosition;
    private float m_StepCycle;
    private float m_NextStep;
    private bool m_Jumping;
    private AudioSource m_AudioSource;
    private Animator anim;
    private float speedMultiplier;

    float _restiingTime = 2.5f;
    float _restingElapsed = 0;
    float _recoveryPerCycleTime = 0.25f; // 4 times per second
    bool _staminaIncrese = false;
    //float _maxSpeedModifier = 1; // After exaustion we want to reduce speed slowly
    float _recoveryPerCycleElapsed = 0; // Per recovery wait

    // Use this for initialization
    private void Start()
    {
        m_CharacterController = GetComponent<CharacterController>();
        m_Camera = Camera.main;
        m_OriginalCameraPosition = m_Camera.transform.localPosition;
        m_FovKick.Setup(m_Camera);
        m_HeadBob.Setup(m_Camera, m_StepInterval);
        m_StepCycle = 0f;
        m_NextStep = m_StepCycle / 2f;
        m_Jumping = false;
        m_AudioSource = GetComponent<AudioSource>();
        mouseLook.Init(transform, m_Camera.transform);
        anim = GetComponent<Animator>();
    }


    // Update is called once per frame
    private void Update()
    {
        if (enemy.playerAlive)
        {
            RotateView();
        }
        // the jump state needs to read here to make sure it is not missed
        if (!m_Jump)
        {
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }

        if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
        {
            StartCoroutine(m_JumpBob.DoBobCycle());
            PlayLandingSound();
            m_MoveDir.y = 0f;
            m_Jumping = false;
        }
        if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
        {
            m_MoveDir.y = 0f;
        }

        //if(!m_IsExausted)
        if(m_StaminaController.UseStamina || m_StaminaLeft < 1 )
            UpdateStamina();

        //Debug.LogError(m_IsRunning);

        m_PreviouslyGrounded = m_CharacterController.isGrounded;
    }


    private void PlayLandingSound()
    {
        m_NextStep = m_StepCycle + .5f;
    }


    private void FixedUpdate()
    {
        float speed;

        if (m_CanMove && !m_IsExausted)
            GetInput(out speed);
        else
            speed = 0;

        // always move along the camera forward as it is the direction that it being aimed at
        Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

        // get a normal for the surface that is being touched to move along it
        RaycastHit hitInfo;
        Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                           m_CharacterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
        // desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

        m_MoveDir.x = desiredMove.x * speed;
        m_MoveDir.z = desiredMove.z * speed;


        if (m_CharacterController.isGrounded)
        {
            m_MoveDir.y = -m_StickToGroundForce;

            if (m_Jump)
            {
                m_MoveDir.y = m_JumpSpeed;
                m_Jump = false;
                m_Jumping = true;
            }
        }
        else
        {
            m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
        }
        if (gameObject.GetComponent<CharacterController>().enabled == true)
        {
            m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.deltaTime);
        }

        // ProgressStepCycle(speed);
        UpdateCameraPosition(speed);

        mouseLook.UpdateCursorLock();
    }

    private void UpdateStamina() 
    {
        //Debug.LogError("Update01");
        if (m_IsRunning && m_StaminaController.UseStamina)
        {
            if (m_StaminaLeft > 0)
                m_StaminaLeft -= (Time.deltaTime / m_StaminaDrainTime);
            else if (m_StaminaLeft != 0f)
                m_StaminaLeft = 0;
        }
        else if (_staminaIncrese)
        {
            //Debug.LogError("Update02");
            if (m_StaminaLeft < 1f)
            {
                RecoverStamina();
            }
            else if (m_StaminaLeft != 1f)
            {
                _recoveryPerCycleElapsed = 0;
                m_StaminaLeft = 1f;
            }
        }

        //if (m_StaminaLeft < 1)
        //{
        //    m_StaminaMeter.color = new Color(1f - m_StaminaLeft, m_StaminaLeft, 0, m_StaminaController.UseStamina ? 1f : 0.5f);
        //}
        //else
        //{
        //    m_StaminaMeter.color = new Color(Color.white;
        //}

        if (m_IsResting)
        {
            _restingElapsed += Time.deltaTime;
            if (_restingElapsed > _restiingTime)
            {
                _staminaIncrese = true;
            }
        }
        else
        {
            _staminaIncrese = false;
            _restingElapsed = 0;
        }

        if (m_StaminaController.UseStamina)
        {

            if (m_StaminaLeft <= 0 && m_IsExausted == false)
            {
                m_IsExausted = true;
                StartCoroutine(coroutine_Exausted());
                //StartCoroutine(coroutine_ExaustionSpeedDrain());
            }
        }

        m_StaminaMeter.fillAmount = m_StaminaLeft;
    }

    private void RecoverStamina() 
    {
        if (_recoveryPerCycleElapsed < _recoveryPerCycleTime)
        {
            // Increase elapsed time               
            _recoveryPerCycleElapsed += Time.deltaTime;
        }
        else
        {
            // Increase stamina
            m_StaminaLeft += (m_StaminaRechargePerCycle / m_TotalStamina) * _recoveryPerCycleTime;
            _recoveryPerCycleElapsed = 0;
        }
    }

    private void ProgressStepCycle(float speed)
    {
        if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
        {
            m_StepCycle += (m_CharacterController.velocity.magnitude + (speed/* *(m_IsWalking ? 0f : m_RunstepLenghten)*/)) *
                         Time.fixedDeltaTime;
        }

        if (!(m_StepCycle > m_NextStep))
        {
            return;
        }

        // m_NextStep = m_StepCycle + m_StepInterval;


    }

    private void UpdateCameraPosition(float speed)
    {
        Vector3 newCameraPosition;
        if (!m_UseHeadBob)
        {
            return;
        }
        if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded)
        {
            m_Camera.transform.localPosition =
                m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude +
                                  (speed/* *(m_IsWalking ? 1f : m_RunstepLenghten)*/));
            newCameraPosition = m_Camera.transform.localPosition;
            newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
        }
        else
        {
            newCameraPosition = m_Camera.transform.localPosition;
            newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
        }
        m_Camera.transform.localPosition = newCameraPosition;
    }


    private void GetInput(out float speed)
    {
        float horizontal;
        float vertical;
        // Read input
        if (useKeyboard)
        {
            horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            vertical = CrossPlatformInputManager.GetAxis("Vertical");
        }
        else
        {
            horizontal = RunAxis.x;
            vertical = RunAxis.y;
        }


        if (enemy.playerAlive && scorpion.playerAlive)
        {
            if (horizontal > 0 || vertical > 0)
            {
                if (!GetComponent<Player>().isHiding)
                {
                    if (horizontal > 0.85f || vertical > 0.85f)
                    {
                        anim.SetBool("Running", true);
                        anim.SetBool("Walking", false);

                        if (m_StaminaController.UseStamina)
                        {
                            if (!m_IsExausted) speedMultiplier = 1.2f;
                        }
                        else 
                        {
                            speedMultiplier = 0.9f;
                        }

                        m_IsRunning = true;
                    }
                    else
                    {
                        anim.SetBool("Running", false);
                        anim.SetBool("Walking", true);
                        if (!m_IsExausted)  speedMultiplier = 0.7f;
                        m_IsRunning = false;
                    }


                    if (horizontal > 0.5f || vertical > 0.5f)
                    {
                        anim.speed = 1f;
                    }
                    else
                    {
                        anim.speed = 0.6f;
                    }
                }
            }
            else
            {
                anim.SetBool("Running", false);
                anim.SetBool("Walking", false);
                m_IsRunning = false;
            }
        }
        else
        {
            anim.SetBool("Running", false);
            anim.SetBool("Walking", false);
            m_IsRunning = false;
        }

        bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
            m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif

        speed = forwardSpeed;
        // set the desired speed to be walking or running
        if (horizontal > 0 || horizontal < 0)
        {
            //strafe
            speed = sideSpeed;
        }
        if (vertical < 0)
        {
            //backwards
            speed = backwardSpeed;
        }
        if (vertical > 0)
        {
            //forwards
            //handled last as if strafing and moving forward at the same time forwards speed should take precedence
            speed = forwardSpeed;
        }


        m_Input = new Vector2(horizontal, vertical);

        // normalize input if it exceeds 1 in combined length:
        if (m_Input.sqrMagnitude > 1)
        {
            m_Input.Normalize();
        }

        // handle speed change to give an fov kick
        // only if the player is going to a run, is running and the fovkick is to be used
        if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
        {
            StopAllCoroutines();
            StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
        }

        //if (m_StaminaLeft < 0.05f && !m_IsExausted) 
        //{

        //    m_IsExausted = true;
        //    StartCoroutine(coroutine_PlayerExausted());

        //}

        //Debug.Log("X: " + horizontal + " | Y: " + vertical);

        if (horizontal < 0.2f && vertical < 0.2f)
            m_IsResting = true;
        else
            m_IsResting = false;

        speed = speed * speedMultiplier;
    }

    private void RotateView()
    {
        mouseLook.LookRotation(transform, m_Camera.transform, useKeyboard);
    }


    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        //dont move the rigidbody if the character is on top of it
        if (m_CollisionFlags == CollisionFlags.Below)
        {
            return;
        }

        if (body == null || body.isKinematic)
        {
            return;
        }
        body.AddForceAtPosition(m_CharacterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
    }

    //IEnumerator coroutine_PlayerExausted() 
    //{
    //    float oldrechargeTime = m_StaminaRechargeTime;
    //    m_StaminaRechargeTime = 10000000000;
    //    float timer = 4;
    //    float x = 0;

    //    while (x < timer) 
    //    {
    //        Debug.Log(x);
    //        speedMultiplier = speedMultiplier > 0 ? speedMultiplier - Time.deltaTime : 0;
    //        x += Time.deltaTime;
    //        yield return null;
    //    }

    //    yield return new WaitForSeconds(4f);

    //    m_StaminaRechargeTime = oldrechargeTime;
    //    m_IsExausted = false;
    //}

    IEnumerator coroutine_Exausted() 
    {
        anim.SetTrigger("Exausted");
        anim.SetBool("Running", false);
        anim.SetBool("Walking", false);
        m_IsResting = true;
        m_IsRunning = false;
        staminaIcon.SetActive(false);
        staminaWarning.SetActive(true);
        //m_CanMove = false;

        yield return new WaitForSeconds(12f);
        m_StaminaLeft += (m_StaminaRechargePerCycle / m_TotalStamina) * _recoveryPerCycleTime;
        m_IsExausted = false;
        m_CanMove = true;
        //_maxSpeedModifier = 1;
        staminaIcon.SetActive(true);
        staminaWarning.SetActive(false);

    }

    //IEnumerator coroutine_ExaustionSpeedDrain()
    //{
    //    while (_maxSpeedModifier > 0)
    //    {
    //        _maxSpeedModifier -= Time.deltaTime / 5;
    //        yield return null;
    //    }

    //}
}

