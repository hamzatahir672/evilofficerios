﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "LanguageAsset")]
public class LanguageAsset : ScriptableObject
{
    public LanguageInfo[] Languages;

    [System.Serializable]
    public class LanguageInfo 
    {
        public string LanguageName;
        public bool AllowLanguage;
        public string languageCode;
        public Font languageFont;
    }
}


