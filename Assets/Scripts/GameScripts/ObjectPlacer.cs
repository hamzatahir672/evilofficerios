﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlacer : MonoBehaviour
{

    public bool testMode = false;

    [SerializeField] Transform Well_Pos;
    [SerializeField] Transform[] Petrol_Pos; // Sewers(I) | Backyard(M)
    [SerializeField] Transform[] Bucket_Pos; // Bathroom | Backyard(M)
    [SerializeField] Transform[] Book_Pos; // Bookshelf(B) | Sewers(I)
    [SerializeField] Transform[] ScrewDriver_Pos; // CabinetTV | Computer      
    [SerializeField] Transform[] BigOBJ_Pos; // Barrels | BasementTable | BackyardCabin
    [SerializeField] Transform[] KeyDrawer_Pos; // Enterance | Cell_Right
    [SerializeField] Transform[] Key_Pos; // Grave(I) | Fridge | TV | CabinetComputer | LockerBlue | Dustbin | Sewers(I)
    [SerializeField] Transform[] Code_Pos; // Helicopter(H) | Secret_Room(S) | Bookshelf(B) | Sewers(I)
    [SerializeField] Transform[] Pryer_Pos; // Secret_Room(S) | Computer | Computer_Cabinet | Computer_BlueLocker
    [SerializeField] Transform[] StairPiece_Pos; // TV_Cabinet | TV_Locker | Fridge | Grave
    [SerializeField] Transform[] Welder_Pos; // Backyard(B) | TV_BlueLocker | Computer | Helicopter(H)
    [SerializeField] Transform[] FlashLight_Pos; // Backyard(B) | TV_BlueLocker | Computer | Helicopter(H)
    [SerializeField] Transform[] Heli_Test_Pos; // 4 pos
    [SerializeField] Transform[] Door_Test_Pos; // 5 pos

    [SerializeField] GameObject wellWater;
    [SerializeField] GameObject[] drawerWithObjs;

    [SerializeField] GameObject Bucket;
    [SerializeField] GameObject SercretBook;
    [SerializeField] GameObject Petrol;
    [SerializeField] GameObject Code;
    [SerializeField] GameObject HeliKey;
    [SerializeField] GameObject BookshelfKey;
    [SerializeField] GameObject LockerKey;
    [SerializeField] GameObject GateKey;
    [SerializeField] GameObject Screwdriver;
    [SerializeField] GameObject HeliHandle;
    [SerializeField] GameObject Axe;
    [SerializeField] GameObject Shovel;
    [SerializeField] GameObject HeliGuide;
    [SerializeField] GameObject Welder;
    [SerializeField] GameObject StairPiece;
    [SerializeField] GameObject IronPryer;
    [SerializeField] GameObject FlashLight;

    // Start is called before the first frame update
    void Start()
    {
        if (testMode)
        {
            SetTestPos();
        }
        else 
        {
            int posInt = Random.Range(1, 5);
            //int posInt = 4;

            switch (posInt)
            {
                case 1 :
                SetPos01();
                break;
            
                case 2 :
                SetPos02();
                break;

                case 3 :
                SetPos03();
                break;

                case 4 :
                SetPos04();
                break;
            }
        
        }

    }

    

    void SetPos01()
    {
        SetPosWithParent(Bucket, Bucket_Pos[0]);
        SetPosWithParent(SercretBook, Book_Pos[0]);
        SetPosWithParent(Petrol, Petrol_Pos[0]);//
        SetPosWithParent(Code, Code_Pos[0]);
        SetPosWithParent(HeliKey, Key_Pos[0]);
        SetPosWithParent(BookshelfKey, Key_Pos[4]);
        SetPosWithParent(LockerKey, Key_Pos[3]);
        SetPosWithParent(Screwdriver, ScrewDriver_Pos[0]);
        SetPosWithParent(HeliHandle, BigOBJ_Pos[0]);
        SetPosWithParent(Axe, BigOBJ_Pos[1]);
        SetPosWithParent(Shovel, BigOBJ_Pos[2]);
        SetPosWithParent(HeliGuide, Well_Pos, wellWater);
        SetPosWithParent(StairPiece, StairPiece_Pos[1]);
        SetPosWithParent(IronPryer, Pryer_Pos[0]);
        SetPosWithParent(Welder, Welder_Pos[0]);

        if (PlayerPrefs.GetString("GameMode") == "Nightmare") 
        {
            SetPosWithParent(FlashLight, FlashLight_Pos[0]);
        }

    }

    void SetPos02()
    {
        SetPosWithParent(Bucket, Bucket_Pos[0]);
        SetPosWithParent(SercretBook, Book_Pos[0]);
        SetPosWithParent(Petrol, Petrol_Pos[1]);
        SetPosWithParent(Code, KeyDrawer_Pos[0], drawerWithObjs[0], false);
        SetPosWithParent(HeliKey, Key_Pos[6]);//
        SetPosWithParent(BookshelfKey, Key_Pos[1]);
        SetPosWithParent(LockerKey, Key_Pos[0]);
        SetPosWithParent(Screwdriver, Well_Pos, wellWater, false);
        SetPosWithParent(HeliHandle, BigOBJ_Pos[0]);
        SetPosWithParent(Axe, BigOBJ_Pos[1]);
        SetPosWithParent(Shovel, BigOBJ_Pos[2]);
        SetPosWithParent(HeliGuide, Code_Pos[1]);
        SetPosWithParent(StairPiece, StairPiece_Pos[2]);
        SetPosWithParent(IronPryer, Pryer_Pos[1]);
        SetPosWithParent(Welder, Welder_Pos[1]);

        if (PlayerPrefs.GetString("GameMode") == "Nightmare")
        {
            SetPosWithParent(FlashLight, FlashLight_Pos[0]);
        }
    }

    void SetPos03()
    {
        SetPosWithParent(Bucket, Bucket_Pos[1]);
        SetPosWithParent(SercretBook, Book_Pos[1]);//
        SetPosWithParent(Petrol, Petrol_Pos[0]);
        SetPosWithParent(Code, Code_Pos[2]);
        SetPosWithParent(HeliKey, Key_Pos[2]);
        SetPosWithParent(BookshelfKey, Key_Pos[0]);
        SetPosWithParent(LockerKey, Key_Pos[5]);
        SetPosWithParent(Screwdriver, ScrewDriver_Pos[1]);
        SetPosWithParent(HeliHandle, BigOBJ_Pos[1]);
        SetPosWithParent(Axe, BigOBJ_Pos[0]);
        SetPosWithParent(Shovel, BigOBJ_Pos[2]);
        SetPosWithParent(HeliGuide, Well_Pos, wellWater);
        SetPosWithParent(StairPiece, StairPiece_Pos[3]);
        SetPosWithParent(StairPiece, StairPiece_Pos[0]);
        SetPosWithParent(IronPryer, Pryer_Pos[2]);
        SetPosWithParent(Welder, Welder_Pos[3]);

        if (PlayerPrefs.GetString("GameMode") == "Nightmare")
        {
            SetPosWithParent(FlashLight, FlashLight_Pos[1]);
        }
    }

    void SetPos04()
    {
        SetPosWithParent(Bucket, Bucket_Pos[0]);
        SetPosWithParent(SercretBook, Book_Pos[0]);
        SetPosWithParent(Petrol, Petrol_Pos[1]);
        SetPosWithParent(Code, Code_Pos[3]);//
        SetPosWithParent(HeliKey, Key_Pos[1]);
        SetPosWithParent(BookshelfKey, Key_Pos[4]);
        SetPosWithParent(LockerKey, KeyDrawer_Pos[1], drawerWithObjs[1]);
        SetPosWithParent(Screwdriver, ScrewDriver_Pos[1]);
        SetPosWithParent(HeliHandle, BigOBJ_Pos[2]);
        SetPosWithParent(Axe, Well_Pos, wellWater,false);
        SetPosWithParent(Shovel, BigOBJ_Pos[0]);
        SetPosWithParent(HeliGuide, BigOBJ_Pos[1]);
        SetPosWithParent(StairPiece, StairPiece_Pos[3]);
        SetPosWithParent(IronPryer, Pryer_Pos[3]);
        SetPosWithParent(Welder, Welder_Pos[2]);

        if (PlayerPrefs.GetString("GameMode") == "Nightmare")
        {
            SetPosWithParent(FlashLight, FlashLight_Pos[1]);
        }
    }

    void SetTestPos() 
    {
        SetPosWithParent(Bucket, Bucket_Pos[0]);
        SetPosWithParent(SercretBook, Door_Test_Pos[0]);
        SetPosWithParent(Petrol, Heli_Test_Pos[0]);
        SetPosWithParent(Code, Door_Test_Pos[1]);
        SetPosWithParent(HeliKey, Heli_Test_Pos[1]);
        SetPosWithParent(BookshelfKey, Key_Pos[4]);
        SetPosWithParent(GateKey, Door_Test_Pos[2]);
        SetPosWithParent(Screwdriver, Door_Test_Pos[3]);
        SetPosWithParent(HeliHandle, Heli_Test_Pos[2]);
        SetPosWithParent(Axe, Door_Test_Pos[4]);
        SetPosWithParent(Shovel, BigOBJ_Pos[0]);
        SetPosWithParent(HeliGuide, Heli_Test_Pos[3]);
    }


    void SetPosWithParent(GameObject obj, Transform target, GameObject parentObj = null, bool changeRotation = true)
    {
        obj.transform.position = target.position;

        if(changeRotation) { obj.transform.rotation = target.rotation; }

        if(parentObj != null) { obj.transform.parent = parentObj.transform; }
        
    }
}
