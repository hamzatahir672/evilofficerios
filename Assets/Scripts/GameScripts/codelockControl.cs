﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class codelockControl : MonoBehaviour
{

    [HideInInspector]
    public int a = 0;
    [HideInInspector]
    public int b = 0;
    [HideInInspector]
    public int c = 0;
    [HideInInspector]
    public int d = 0;

    public int codeA = 1;
    public int codeB = 9;
    public int codeC = 1;
    public int codeD = 3;
    public AudioClip correctPass;
    public AudioClip wrongPass;

    private AudioSource source;
    
    void Start()
    {
        source = GetComponent<AudioSource>();
    }
    public void resetLock()
    {
        a = 0;
        b = 0;
        c = 0;
        d = 0;
        source.clip = wrongPass;
        source.Play();
        Debug.Log("Wrong Password");
    }

    public void openLock()
    {
        GameObject[] buttons = GameObject.FindGameObjectsWithTag("KeypadBtn");

        foreach (GameObject button in buttons)
        {
            button.tag = "Untagged";
        }
        gameObject.GetComponentInParent<LockInteraction>().locked = false;
        
        gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.green);
        gameObject.GetComponentInParent<LockInteraction>().UnlockObject();
        source.clip = correctPass;
        source.Play();
    }
}
