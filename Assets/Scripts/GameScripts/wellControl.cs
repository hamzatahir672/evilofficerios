﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wellControl : MonoBehaviour
{
    [SerializeField] Transform levelOne;
    [SerializeField] Transform levelTwo;
    [SerializeField] Transform levelThree;
    [SerializeField] GameObject wellWater;
    int wellLevel = 0;

    public void Fill()
    {
        wellLevel++;

        if(wellLevel == 1)
        {
            wellWater.transform.position = levelOne.position;
        } 
        else if(wellLevel == 2)
        {
            wellWater.transform.position = levelTwo.position;
        } 
        else if(wellLevel == 3)
        {
            wellWater.transform.position = levelThree.position;
            gameObject.tag = "Untagged";
        }
        else
        {
            wellLevel--;
        }
    }
}
