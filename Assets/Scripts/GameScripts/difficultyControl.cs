﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class difficultyControl : MonoBehaviour
{
    private string difficulty;
    [SerializeField]
    private EnemyAI enemy;
    [SerializeField]
    private GameObject CrackingPoints;
    [SerializeField]
    private GameObject scorpionArea;
        


    private void Start()
    {
        difficulty = PlayerPrefs.GetString("Difficulty");

        if (difficulty == "Ghost")
        {
            RenderSettings.fogDensity = 0.1f;
            enemy.RunSpeed = 1.5f;
            enemy.animRunSpeed = 1.5f;
            CrackingPoints.SetActive(false);
            scorpionArea.SetActive(false);
        }
        else if (difficulty == "Practice")
        {
            RenderSettings.fogDensity = 0.1f;
            enemy.gameObject.SetActive(false);
            CrackingPoints.SetActive(false);
        }
        else if (difficulty == "Easy")
        {
            enemy.RunSpeed = 1.5f;
            enemy.animRunSpeed = 1.5f;
            RenderSettings.fogDensity = 0.1f;
            CrackingPoints.SetActive(false);

        }
        else if (difficulty == "Normal")
        {
            enemy.RunSpeed = 2f;
            enemy.animRunSpeed = 2f;
            RenderSettings.fogDensity = 0.12f;
        }
        else if (difficulty == "Hard")
        {
            enemy.RunSpeed = 2.75f;
            enemy.animRunSpeed = 2.75f;
            RenderSettings.fogDensity = 0.15f;
        }
        else if (difficulty == "Extreme")
        {
            enemy.RunSpeed = 3.5f;
            enemy.animRunSpeed = 3.5f;
            RenderSettings.fogDensity = 0.2f;
        }
        else if (difficulty == "Nightmare")
        {
            enemy.RunSpeed = 4f;
            enemy.animRunSpeed = 4f;
            RenderSettings.fogDensity = 0.22f;
        }
        else 
        {
            enemy.RunSpeed = 1f;
            enemy.animRunSpeed = 1f;
            RenderSettings.fogDensity = 0.22f;
            Debug.LogError("Difficulty not selected!! Please select from the MaIn Menu");
        }
    }

    // Start is called before the first frame update
    // void Awake()
    // {
    //     difficulty = PlayerPrefs.GetString("Difficulty");
    //     enemy1 = GameObject.Find("Enemy1");
    //     enemy2 =  GameObject.Find("Enemy2");
    //     if(difficulty == "Extreme" || difficulty == "Nightmare")
    //     {
    //         gameObject.GetComponent<playerCaught>().enemy = enemy2.GetComponent<EnemyAI>();
    //         gameObject.GetComponent<startDay>().enemy = enemy2.GetComponent<EnemyAI>();
    //         gameObject.GetComponent<gameComplete>().Enemy = enemy2;
    //     }
    //     else
    //     {
    //         gameObject.GetComponent<playerCaught>().enemy = enemy1.GetComponent<EnemyAI>();
    //         gameObject.GetComponent<startDay>().enemy = enemy1.GetComponent<EnemyAI>();
    //         gameObject.GetComponent<gameComplete>().Enemy = enemy1;
    //     }
    // }


}
