﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class electricboxControl : MonoBehaviour
{
    [SerializeField] LockInteraction screw1;
    [SerializeField] LockInteraction screw2;
    [SerializeField] LockInteraction screw3;
    [SerializeField] LockInteraction screw4;

    public void openBox()
    {
        if(!screw1.locked && !screw2.locked && !screw3.locked && !screw4.locked)
        {
            gameObject.AddComponent<Rigidbody>();
        }
    }

}
