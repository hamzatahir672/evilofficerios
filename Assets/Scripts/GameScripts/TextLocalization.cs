﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLocalization : MonoBehaviour
{
    //[Tooltip("0 for EN. 1 for FR. 2 for UR. 3 for HN. 4 for SP. 5 for IT")]
    //public string[] TextValues;
    [SerializeField] string TextKey;
    [SerializeField] Text txt;

    // Start is called before the first frame update
    void Awake()
    {
        txt = GetComponent<Text>();

    }

    public void UpdateText(Font languageFont) 
    {
        if(txt == null)
            txt = GetComponent<Text>();
        //Debug.Log(languageFont);
        //txt.font = languageFont;

        if (Localization.LanguageJSON[PlayerPrefs.GetString("Language")][TextKey] != "" && !string.IsNullOrEmpty(Localization.LanguageJSON[PlayerPrefs.GetString("Language")][TextKey]))
            txt.text = Localization.LanguageJSON[PlayerPrefs.GetString("Language")][TextKey];
    }


}
