﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    public bool seePlayer;
    public bool playerHiding;
    private bool caughtPlayerHidng = false;
    public bool playerAlive = true;
    public bool canMove = false;
    public GameObject target;
    public float seeTimer;
    public Transform[] navPos;
    public AudioClip[] footSound;
    public Transform heardObjectPos;
    public int targetNav;
    public string state = "idle";
    public float speed;
    public float animspeed;
    public float RunSpeed;
    public float animRunSpeed;
    public new AudioManager audio;
    

    private string difficulty;
    private Animator anim;
    private NavMeshAgent navAgent;
    private AudioSource source;
    public AudioSource dandaSource;
    private float waitTimer = 0f;
    private RaycastHit hitInfo;
    private float doorTimer = 2f;
    public bool openingDoor = false;
    private bool seeDoor;
    private bool heardObject = false;
    private bool isChasing = false;
    public int audioIndex = 0;
    

    
    // Start is called before the first frame update
    void Start()
    { 
        source = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        navAgent = gameObject.GetComponent<NavMeshAgent>();
        // navAgent.stoppingDistance = gameObject.GetComponentInChildren<EnemyEye>().attackDistance;
        
    }

    // Update is called once per frame
    void Update()
    {   
        navAgent.speed = speed;
        anim.speed = animspeed;
        anim.SetFloat("velocity",navAgent.velocity.magnitude);
        if(playerAlive)
        {
            if(canMove)
            {
                if(Physics.Raycast(new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z) ,transform.forward, out hitInfo , 1f))
                {
                    if(!seePlayer)
                    {
                        if(hitInfo.collider.gameObject.tag == "Door")
                        {
                            seeDoor = true;
                            if(!hitInfo.collider.gameObject.GetComponent<doorControl>().open)
                            {
                                if(doorTimer > 0)
                                {
                                    doorTimer-= Time.deltaTime;
                                    openingDoor = true;
                                    navAgent.speed = 0f;
                                }
                                if(doorTimer <= 0f)
                                {
                                    hitInfo.collider.gameObject.GetComponent<doorControl>().ChangeDoorState();
                                    openingDoor = false;
                                    doorTimer = 2f;
                                }
                            }
                            
                        } 
                        else if(seeDoor || openingDoor)
                        {
                            seeDoor = false;
                            openingDoor = false;
                        }
                    }
                }
                else if(seeDoor || openingDoor)
                {
                    seeDoor = false;
                    openingDoor = false;
                }

                if (seePlayer || seeTimer > 0)
                {
                    if (state != "sawHiding")
                    {
                        navAgent.destination = target.transform.position;
                        seeTimer -= Time.deltaTime;
                        state = "chasing";
                    }
                }

                if(state == "idle")
                {   
                    Idle();
                }

                if(state == "walk")
                {
                    Walk();
                }

                if(state == "search")
                {
                    Search();
                }
                    
                if(state == "chasing")
                {   
                    Chase();
                }
                if(state == "find")
                {   
                    Find();
                }

                if(state == "heard")
                {   
                    Heard();
                }

                if (state == "sawHiding")
                {
                    SawHiding();
                }
            } 
            else
            {
                animspeed = 1f;
                speed = 0f;
            } 
        }
        else
        {
            animspeed = 1f;
            speed = 0f;
        }
    }

    void Idle()
    {
        if(audioIndex != 0)
		{
			audioIndex = 0;
			audio.StopClip();
		}
		audio.StopClip();
		

        animspeed = 1f;
        speed = 1f;
        targetNav = Random.Range(0,navPos.Length);
        navAgent.SetDestination(navPos[targetNav].position);
        // anim.SetFloat("velocity", 1f);
        state = "walk";
    }

    void Walk()
    {
        if(!openingDoor)
        {
            animspeed = 1f;
            speed = 1f;
        }
        if(navAgent.remainingDistance <= navAgent.stoppingDistance && !navAgent.pathPending)
        {
            state = "search";
            waitTimer = 3f;
        }
    }

    void Search()
    {
        if(waitTimer > 0f)
        {
            waitTimer -= Time.deltaTime;
            // anim.SetFloat("velocity", 0f);
        }
        else
        {
            if(difficulty == "Nightmare")
            {
                if(isChasing)
                {
                    navAgent.destination = target.transform.position; 
                    state = "find";
                    waitTimer = 3f;
                    isChasing = false;
                }
                else
                {
                    state = "idle";
                }
                
            }
            else
            {
            state = "idle";
            }
        }
    }

    void Chase()
    {
        if(audioIndex != 1)
		{
			audioIndex = 1;
			audio.PlayClip(audioIndex);
		}
        isChasing = true;

        if(!openingDoor)
        {
            animspeed = animRunSpeed;
            speed = RunSpeed;
        }

        if (!target.GetComponent<Player>().hiddenFromEnemy && playerHiding) //If enemy has seen player hiding
        {
            navAgent.destination = target.GetComponent<Player>().hiddenObject.GetComponent<hideControl>().standPos.position;
            state = "sawHiding";
        }
        else if(!seePlayer && seeTimer <= 0)
        {
            if(navAgent.remainingDistance <= navAgent.stoppingDistance && !navAgent.pathPending)
            {
                state = "search";
                waitTimer = 3f;
            }
        }
    }

    void SawHiding()
    {
        if (navAgent.remainingDistance <= navAgent.stoppingDistance && !navAgent.pathPending)
        {
            if (!caughtPlayerHidng) 
            {
                StartCoroutine(coroutine_CaughtHiding());
                caughtPlayerHidng = true;
            }
        }
        else if (!target.GetComponent<Player>().isHiding) 
        {
            state = "chase";
        }
    }


    void Find()
    {     

        if(navAgent.remainingDistance <= navAgent.stoppingDistance && !navAgent.pathPending)
        {
            if(waitTimer > 0f)
            {
            waitTimer -= Time.deltaTime;
            }
            else
            {
            state = "idle";
            }
        }
        
    }
    void Heard()
    {
        if(heardObject == false)
        {
            navAgent.SetDestination(heardObjectPos.position);
            navAgent.SetDestination(heardObjectPos.position);
            navAgent.SetDestination(heardObjectPos.position);
            navAgent.SetDestination(heardObjectPos.position);
            navAgent.destination = heardObjectPos.position;
            heardObject = true;
        }
        
        if(!openingDoor)
        {
            // anim.SetFloat("velocity", 1f);
            speed = RunSpeed;
            // animspeed = animRunSpeed;
        }

        if(navAgent.remainingDistance <= navAgent.stoppingDistance && !navAgent.pathPending)
            {
                state = "search";
                waitTimer = 3f;
                heardObject = false;
            }
    }
    public void footSteps(int clip)
    {
        source.clip = footSound[clip];
        source.Play();
    }

    public void dandaHit()
    {
        dandaSource.Play();
        target.GetComponent<Animation>().Play("hitDanda");
    }

    IEnumerator coroutine_CaughtHiding() 
    {

        yield return new WaitForSeconds(1f);
        target.GetComponent<Player>().HidePlayer();

    
    }
}
