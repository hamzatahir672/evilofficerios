﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayMessageController : MonoBehaviour
{
    [Header("ENGLISH")]
    [SerializeField]string HeliEscapeTextEN;
    [SerializeField]string SewerEscapeTextEN;
    [SerializeField]string LockedDoorTextEN;
    [SerializeField]string GunNeedTextEN;
    [SerializeField]string GunFullTextEN;
    [SerializeField]string NeedAmmoTextEN;
    [SerializeField]string WellTextEN;
    [SerializeField]string InspectGoneEN;

    [Header("FRENCH")]
    [SerializeField]string HeliEscapeTextFR;
    [SerializeField] string SewerEscapeTextFR;
    [SerializeField]string LockedDoorTextFR;
    [SerializeField]string GunNeedTextFR;
    [SerializeField]string GunFullTextFR;
    [SerializeField]string NeedAmmoTextFR;
    [SerializeField]string WellTextFR;
    [SerializeField]string InspectGoneFR;

    [Header("URDU")]
    [SerializeField]string HeliEscapeTextUR;
    [SerializeField] string SewerEscapeTextUR;
    [SerializeField]string LockedDoorTextUR;
    [SerializeField]string GunNeedTextUR;
    [SerializeField]string GunFullTextUR;
    [SerializeField]string NeedAmmoTextUR;
    [SerializeField]string WellTextUR;
    [SerializeField]string InspectGoneUR;

    [Header("HINDI")]
    [SerializeField]string HeliEscapeTextHN;
    [SerializeField] string SewerEscapeTextHN;
    [SerializeField]string LockedDoorTextHN;
    [SerializeField]string GunNeedTextHN;
    [SerializeField]string GunFullTextHN;
    [SerializeField]string NeedAmmoTextHN;
    [SerializeField]string WellTextHN;
    [SerializeField]string InspectGoneHN;
    [Header("SPANISH")]
    [SerializeField] string HeliEscapeTextSP;
    [SerializeField] string SewerEscapeTextSP;
    [SerializeField] string LockedDoorTextSP;
    [SerializeField] string GunNeedTextSP;
    [SerializeField] string GunFullTextSP;
    [SerializeField] string NeedAmmoTextSP;
    [SerializeField] string WellTextSP;
    [SerializeField] string InspectGoneSP;
    [Header("ITALIAN")]
    [SerializeField] string HeliEscapeTextIT;
    [SerializeField] string SewerEscapeTextIT;
    [SerializeField] string LockedDoorTextIT;
    [SerializeField] string GunNeedTextIT;
    [SerializeField] string GunFullTextIT;
    [SerializeField] string NeedAmmoTextIT;
    [SerializeField] string WellTextIT;
    [SerializeField] string InspectGoneIT;

    [HideInInspector]public string HeliEscapeFinal;
    [HideInInspector] public string SewerEscapeFinal;
    [HideInInspector]public string LockedDoorFinal;
    [HideInInspector]public string GunNeedFinal;
    [HideInInspector]public string GunFullFinal;
    [HideInInspector]public string NeedAmmoFinal;
    [HideInInspector]public string WellFinal;
    [HideInInspector]public string InspectGoneFinal;

    void Awake()
    {
        SetLanguage();
    }

    void SetLanguage()
    {
        string language = PlayerPrefs.GetString("Language", "EN");

        HeliEscapeFinal = Localization.LanguageJSON[language]["HeliEscapeFail"];
        SewerEscapeFinal = Localization.LanguageJSON[language]["SewerEscapeFail"];
        LockedDoorFinal = Localization.LanguageJSON[language]["DoorEscapeFail"];
        GunNeedFinal = Localization.LanguageJSON[language]["GunNeed"];
        GunFullFinal = Localization.LanguageJSON[language]["GunFull"];
        NeedAmmoFinal = Localization.LanguageJSON[language]["NeedAmmo"];
        WellFinal = Localization.LanguageJSON[language]["WellNeed"];
        InspectGoneFinal = Localization.LanguageJSON[language]["InspectorGone"];


    }
}   
