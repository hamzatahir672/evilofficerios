﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject Enemy;
    public GameObject Scorpion;
    public bool hiddenFromEnemy; //Player is hidden From enemy and Enemy cannot see him
    public bool isHiding; //PLayer is currently hiding in a object
    [HideInInspector]public GameObject nearObject;
    public GameObject gameManager;

    [HideInInspector]public GameObject hiddenObject;
    public Text hideBtnText;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            // Player not behind wall
            if(other.gameObject.GetComponent<EnemyEye>().playerInFront)
            {
                Enemy.GetComponent<EnemyAI>().seePlayer = true;
                Enemy.GetComponent<EnemyAI>().seeTimer = 2f;
            }
        }
        if(other.gameObject.tag == "Hide")
        {
            nearObject = other.gameObject;
            GetComponentInChildren<Interaction>().hideBtn.SetActive(true);
            hiddenObject = other.gameObject;
        }
        if(other.gameObject.CompareTag("Hole"))
        {
            Enemy.GetComponent<EnemyAI>().seeTimer = 0f;
            hiddenFromEnemy = true;
            isHiding = true;
            gameManager.GetComponent<playerCrouch>().crouchBtn.gameObject.SetActive(false);
        }
        if(other.gameObject.CompareTag("CrackingPoint"))
        {
            Debug.Log("touched");
            other.gameObject.GetComponent<crackingSound>().playCracking();
        }
        if (other.gameObject.CompareTag("ScorpionSight"))
        {
            Debug.Log("Scorpion Saw You!");
            Scorpion.GetComponent<scorpionAI>().ChangeState("chasing");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Hide"))
        {
            nearObject = null;
            GetComponentInChildren<Interaction>().hideBtn.SetActive(false);
            hiddenObject = null;
        }
        if(other.gameObject.CompareTag("Hole"))
        {
            hiddenFromEnemy = false;
            isHiding = false;
            gameManager.GetComponent<playerCrouch>().crouchBtn.gameObject.SetActive(true);
        }
        if (other.gameObject.CompareTag("ScorpionSight"))
        {
            Debug.Log("Scorpion Lost You!");
            Scorpion.GetComponent<scorpionAI>().ChangeState("idle");
        }
    }

    public void HidePlayer()
    {
        if(!isHiding)
        {
            if (!Enemy.GetComponent<EnemyAI>().seePlayer || Enemy.GetComponent<EnemyAI>().seeTimer <= 0)
            {
                //If the enemy is not seeing while player is hiding 
                hiddenFromEnemy = true;
            }
            else 
            {
                hiddenFromEnemy = false;
            }
            Enemy.GetComponent<EnemyAI>().playerHiding = true;
            GetComponent<CharacterController>().enabled = false;
            transform.position = nearObject.GetComponent<hideControl>().hidePos.position;
            GetComponent<CharacterController>().height = 0f;
            hideBtnText.text = "Unhide";
        }
        else
        {
            hiddenFromEnemy = false;
            Enemy.GetComponent<EnemyAI>().playerHiding = false;
            transform.position = nearObject.gameObject.GetComponent<hideControl>().unhidePos.position;
            GetComponent<CharacterController>().enabled = true;
            GetComponent<CharacterController>().height = 1.8f;
            hideBtnText.text = "Hide";
        }
        isHiding = !isHiding;
    }
}
