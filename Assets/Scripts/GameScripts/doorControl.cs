﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class doorControl : MonoBehaviour
{

    public bool open;
    public bool isMainDoor;
    public Vector3 openAngle;
    public Vector3 closeAngle;
    public AudioClip openClip;
    public AudioClip closeClip;

    private AudioSource source;
    private NavMeshObstacle obstacle;

    // Start is called before the first frame update
    void Start()
    {
        open = false;
        source = GetComponent<AudioSource>();
        obstacle = !isMainDoor ? GetComponent<NavMeshObstacle>() : null;
        Quaternion targetRotation01 = Quaternion.Euler(openAngle.x, openAngle.y, openAngle.z);
        Quaternion targetRotation02 = Quaternion.Euler(closeAngle.x, closeAngle.y, closeAngle.z);
    }

    public void ChangeDoorState()
    {
        //Closing Door
        if(open)
        {
            StartCoroutine(playSound(source, closeClip, 0.3f));
        }
        //Opening Door
        else if(!open)
        {
            StartCoroutine(playSound(source, openClip, 0.01f));
        }
        open = !open;
        StartCoroutine(OpenDoor(1f));
    }

   
    // Update is called once per frame
    // void Update()
    // {
        

        
    // }

    public IEnumerator OpenDoor(float timeInterval)
    {
        
		float currentTime = 0;
		float normalizedValue = 0;
        gameObject.GetComponent<BoxCollider>().enabled = false;

        Quaternion targetRotation01 = Quaternion.Euler(openAngle.x, openAngle.y, openAngle.z);
        Quaternion targetRotation02 = Quaternion.Euler(closeAngle.x, closeAngle.y, closeAngle.z);

		while (gameObject.GetComponent<BoxCollider>().enabled == false) {
			currentTime += Time.deltaTime;
			normalizedValue = currentTime / timeInterval;

            if(open)
            {   
            transform.localRotation = Quaternion.Lerp (transform.localRotation, targetRotation01, (Time.deltaTime * 6f));
            //Opening Door
            if(transform.localRotation == targetRotation01 && !isMainDoor)
            {
                obstacle.carving = true;
            }
            
            
            // transform.localRotation = Quaternion.Lerp(transform.localRotation,targetRotation01, 5f * Time.deltaTime);
            } 
        else if(!open)
        {
            transform.localRotation = Quaternion.Lerp (transform.localRotation, targetRotation02, (Time.deltaTime * 6f));

            //Closing Door
            if(transform.localRotation == targetRotation02 && !isMainDoor)
            {
                obstacle.carving = false;
            }
            
            // transform.localRotation = Quaternion.Lerp(transform.localRotation,targetRotation02, 5f * Time.deltaTime);
        } 

        if(transform.localRotation != targetRotation01 && transform.localRotation != targetRotation02)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
        } 
        else if(transform.localRotation == targetRotation01 || transform.localRotation == targetRotation02)
        {
            gameObject.GetComponent<BoxCollider>().enabled = true;
        }

			yield return null;

		}
	}
    IEnumerator playSound(AudioSource audioSource, AudioClip clip, float delay)
    {
        yield return new WaitForSeconds(delay);
        audioSource.clip = clip;
        audioSource.Play();
    }
}
