﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaController : MonoBehaviour
{
    [SerializeField] Image staminaMeter01;
    [SerializeField] Image staminaMeter02;

    public bool UseStamina  { get { return _useStamina; } }
    private bool _useStamina = false;

    private void Start()
    {
        _useStamina = PlayerPrefs.GetInt("Stamina", 1) == 1 ? true : false;
        UpdateStaminaButton();
    }

    public void OnClick_StaminaToggle() 
    {
        _useStamina = !_useStamina;
        UpdateStaminaButton();

    }

    void UpdateStaminaButton()
    {
        if (_useStamina)
        {
            staminaMeter01.color = new Color(staminaMeter01.color.r, staminaMeter01.color.g, staminaMeter01.color.b, 1f);
            staminaMeter02.color = new Color(staminaMeter02.color.r, staminaMeter02.color.g, staminaMeter02.color.b, 1f);
            PlayerPrefs.SetInt("Stamina", 1);
        }
        else
        {
            staminaMeter01.color = new Color(staminaMeter01.color.r, staminaMeter01.color.g, staminaMeter01.color.b, 0.5f);
            staminaMeter02.color = new Color(staminaMeter02.color.r, staminaMeter02.color.g, staminaMeter02.color.b, 0.5f);
            PlayerPrefs.SetInt("Stamina", 0);
        }
    }
}
