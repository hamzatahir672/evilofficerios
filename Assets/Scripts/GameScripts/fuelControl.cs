﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fuelControl : MonoBehaviour
{

    public bool isFilled;
    public Slider fillSlider;

    [SerializeField]
    private ReferenceObj referenceObj;
    private float fillAmount = 0;
    private GameObject requiredItem;

    void Start()
    {
        requiredItem = gameObject.GetComponent<LockInteraction>().requiredItem;
    }
    // Update is called once per frame
    void Update()
    {
        if(referenceObj.fillBtn.activeInHierarchy == false &&
           fillSlider.gameObject.activeInHierarchy == true)
        {
            fillSlider.gameObject.SetActive(false);
        }
    }
    public void FillTank()
    {
        if(referenceObj.playerHead.GetComponent<Interaction>().PickedItem == requiredItem)
        {
            if(!isFilled)
            {
            if(fillSlider.gameObject.activeInHierarchy == false)
            {
                fillSlider.gameObject.SetActive(true);
            }
            fillAmount = fillAmount + Time.deltaTime/10f;
            fillSlider.value = fillAmount;
            if(fillAmount >= 1f)
            {
                gameObject.GetComponent<LockInteraction>().locked = false;
                isFilled = true;
            }
            }
        }
        
        
    }
}
