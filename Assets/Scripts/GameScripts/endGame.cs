﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endGame : MonoBehaviour
{
    public GameObject rateUsPanel;

    public void ReturnToHome()
    {
        SceneManager.LoadScene(0);
    }

    public void ShowRateUs() 
    {
        rateUsPanel.SetActive(true);
    }

    public void RateOnPlayStore()
    {
        Application.OpenURL("market://details?id=" + Application.identifier);
    }
}
