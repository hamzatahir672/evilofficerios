﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class MobileControl : MonoBehaviour
{

    public VariableJoystick MoveJoystick;
    public FixedTouchField Touchfield;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // var fps = GetComponent<FPSPlayerController>();
        var fps = GetComponent<FirstPersonController>();

        fps.RunAxis = new Vector2(MoveJoystick.Horizontal,MoveJoystick.Vertical);
        fps.mouseLook.LookAxis = Touchfield.TouchDist;
    }
}
