﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// [CreateAssetMenu(fileName = "ReferenceObject", menuName = "MyObjects/ReferenceObject", order = 1)]
public class ReferenceObj : MonoBehaviour
{
   public EnemyAI Enemy;
   public EnemyEye eye;
   public GameObject EnemyObj;
   public GameObject enemyHead;
   public GameObject player;
   public GameObject playerHead;
   public Text itemText;
   public Text hintText;
   public GameObject interactionBtn;
   public GameObject dropBtn;
   public GameObject hideBtn;
   public GameObject shootBtn;
   public GameObject fillBtn;
   public GameObject heliGuide;
   public GameObject mainGate;
   public GameObject heliEscape;
   public GameObject sewerEscape;

   
}
