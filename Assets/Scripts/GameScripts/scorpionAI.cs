﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class scorpionAI : MonoBehaviour
{
    public bool canMove = false;
    public float speed = 3f;
    public Transform[] navPos;
    public Animator anim;
    public bool playerAlive = true;
    public float attackDistance;
    public bool isAlive = true;
    public AudioClip[] clips;

    float animspeed = 3f;
    NavMeshAgent navAgent;
    string state = "idle";
    bool isChasing = false;
    float waitTimer = 0f;
    AudioSource source;

    [SerializeField] GameObject target;
    
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        navAgent = GetComponent<NavMeshAgent>();
        source = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        float sqrMagnitude = (this.target.transform.position - transform.position).sqrMagnitude;
        navAgent.speed = speed;
        anim.speed = animspeed;
        //anim.SetFloat("velocity", navAgent.velocity.magnitude);

        if (sqrMagnitude < attackDistance * attackDistance && playerAlive) 
        {
            if (isAlive) 
            {
                anim.speed = 1;
                anim.SetTrigger("death");
                playerAlive = false;


                if (gameController.instance.gameObject.GetComponent<playerCrouch>().playerCrouching)
                {
                    gameController.instance.gameObject.GetComponent<playerCrouch>().Crouch();
                }
                if (target.GetComponentInChildren<Interaction>().isPicking)
                {
                    target.GetComponentInChildren<Interaction>().DropItem();
                }
                source.clip = clips[0];
                source.Play();

                return;
            }
        }

        if (playerAlive)
        {
            if (canMove)
            {

                if (state == "idle")
                {
                    Idle();
                }

                if (state == "walk")
                {
                    Walk();
                }

                if (state == "search")
                {
                    Search();
                }

                if (state == "chasing")
                {
                    Chase();
                }                
            }
            else
            {
                animspeed = 1f;
                speed = 0f;
            }
        }
        else
        {
            animspeed = 1f;
            speed = 0f;
        }
    }

    void Idle()
    {
        anim.SetFloat("velocity", 0.2f);
        speed = 1f;
        int targetNav = Random.Range(0, navPos.Length);
        navAgent.SetDestination(navPos[targetNav].position);
        // anim.SetFloat("velocity", 1f);
        state = "walk";
    }

    void Walk()
    {
        
        if (navAgent.remainingDistance <= navAgent.stoppingDistance && !navAgent.pathPending)
        {
            state = "search";
            waitTimer = 3f;
        }
        else
        {
            anim.SetFloat("velocity", 0.3f);
        }
    }

    void Search()
    {
        if (waitTimer > 0f)
        {
            waitTimer -= Time.deltaTime;
            anim.SetFloat("velocity", 0.2f);
            // anim.SetFloat("velocity", 0f);
        }
        else
        {
            
            state = "idle";
            
        }
    }

    void Chase() 
    {
        anim.SetFloat("velocity", 0.3f);
        navAgent.destination = target.transform.position;
        speed = 2f;
        animspeed = 1.5f;
    }

    public void ChangeState(string _state) 
    {
        this.state = _state;
    }

    public void bitten()
    {
        //dandaSource.Play();
        target.GetComponent<Animation>().Play("hitDanda");
    }

    public void DeathSound() 
    {
        source.clip = clips[1];
        source.Play();

        GetComponent<CapsuleCollider>().enabled = false;
    }
}
//-32.23
