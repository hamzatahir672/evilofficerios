﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interaction : MonoBehaviour
{
    public float distanceToPick;
    public Transform pickUpPoint;
    public Transform dropPoint;
    public Text itemName;
    public Text hintText;
    public GameObject InteractionBtn;
    public GameObject interactionCircle;
    public GameObject ShootBtn;
    public GameObject dropBtn;
    public GameObject hideBtn;
    public GameObject fillBtn;
    public GameObject mainGate;
    public EnemyAI enemy;
    public bool isPicking = false;
    public AudioClip emptyBucketClip;
    public GameObject PickedItem = null;
    public GameObject LeftItem = null;

    private RaycastHit hit;
    private int layermask;
    private string Job;
    
    private GameObject inventoryItem;
    private AudioSource source;
    private ReferenceObj referenceObject;
    private gameController managerReference;
    private string language;

    void Start()
    {
        language = PlayerPrefs.GetString("Language", "EN");
        int mask1 = 1 << 2;
        int mask2 = 1 << 9;
        layermask = mask1 | mask2;
        layermask = ~layermask;
        source = GetComponent<AudioSource>();
        referenceObject = GameObject.Find("ReferenceController").GetComponent<ReferenceObj>();
        managerReference = gameController.instance;
    }
    // Update is called once per frame
    void Update()
    {
        #if UNITY_EDITOR
        Debug.DrawRay(gameObject.transform.position,gameObject.transform.forward * distanceToPick,Color.red);
        #endif

        #region Raycasting
        if(Physics.Raycast(gameObject.transform.position,gameObject.transform.forward, out hit,distanceToPick, layermask))
        {
            //Debug.Log(hit.collider.gameObject.name);
            // Hide Interaction Button
            if (InteractionBtn.activeInHierarchy == true)
            {
                InteractionBtn.SetActive(false);
            }

            // Hide Fill Button
            if(fillBtn.activeInHierarchy == true)
            {
                fillBtn.SetActive(false);
            }
            // Ray Hits Pickable Object
            #region Pickable
            if(hit.collider.gameObject.tag == "Pickable" || hit.collider.gameObject.tag == "Gun")
            {
                //if (hit.collider.name == "FlashLight")
                //{
                //}
                //else
                //{
                    InteractionBtn.SetActive(true);
                    Job = "Pick";
                    if (itemName.IsActive() == false)
                    {
                        itemName.text = Localization.LanguageJSON[PlayerPrefs.GetString("Language")][hit.collider.gameObject.GetComponent<objectControl>().languageKey];
                        itemName.gameObject.SetActive(true);
                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        Interact("Pick");
                    }
                //}
            }
            else 
            {
                if(itemName.IsActive() == true || hit.collider.gameObject == null )
                {
                    itemName.gameObject.SetActive(false);
                }
            }
            #endregion

            // Ray Hits Gun
            #region Bullet
            if(hit.collider.gameObject.tag == "Bullet")
            {
                InteractionBtn.SetActive(true);
                Job = "Bullet";
               
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("Bullet");
                }
            }
            
            #endregion
            
            // Ray Hits Door
            #region  Door
            if(hit.collider.gameObject.tag == "Door" || hit.collider.gameObject.tag == "CabinetDoor")
            {
                Job = "Door";
                InteractionBtn.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("Door");
                }
            }
            #endregion

            // Ray Hits Well
            #region  Well
            if(hit.collider.gameObject.tag == "Well")
            {
                Job = "Well";
                InteractionBtn.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("Well");
                }
            }
            #endregion

            // Ray Hits PowerButton
            #region  PowerButton
            if(hit.collider.gameObject.tag == "PowerButton")
            {
                Job = "PowerButton";
                InteractionBtn.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("PowerButton");
                }
            }
            #endregion

            // Ray Hits KeypadBtn
            #region  KeypadBtn
            if(hit.collider.gameObject.tag == "KeypadBtn")
            {
                Job = "KeypadBtn";
                
                InteractionBtn.SetActive(true);
                managerReference.keypadInfoText.text = hit.collider.gameObject.GetComponent<keypadButton>().btnValue.ToString();
                managerReference.keypadTypedText.text = managerReference.codeLock.a.ToString() + managerReference.codeLock.b.ToString() + managerReference.codeLock.c.ToString() + managerReference.codeLock.d.ToString();
                managerReference.keypadInfoText.gameObject.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("KeypadBtn");
                }
            }
            else
            {
                managerReference.keypadInfoText.gameObject.SetActive(false);
            }
            #endregion

            // Ray Hits Drawer
             #region  Drawer
            if(hit.collider.gameObject.tag == "Drawer")
            {
                Job = "Drawer";
                InteractionBtn.SetActive(true);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("Drawer");
                }
            }
            #endregion

            // Ray Hits Locked Object
            #region  Locked
            if(hit.collider.gameObject.tag == "Locked")
            {
                InteractionBtn.SetActive(true);
                Job = "Locked";
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("Locked");
                }   
            }
            #endregion

            // Ray Hits Main Door
            #region  DoorFinish
            if (hit.collider.gameObject.tag == "MainGate")
            {
                interactionCircle.SetActive(true);
                InteractionBtn.SetActive(true);
                Job = "Finish";
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("Finish");
                }
            }
            else
            {
                interactionCircle.SetActive(false);
            }
            #endregion

            // Ray Hits HeliEscape
            #region  HeliFinish
                if (hit.collider.gameObject.tag == "HeliFinish")
            {
                interactionCircle.SetActive(true);
                InteractionBtn.SetActive(true);
                Job = "HeliEscape";
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("HeliEscape");
                }
            }
            else 
            {
                interactionCircle.SetActive(false);
            }
            #endregion

            // Ray Hits SewerEscape
            #region  SewerFinish
            if (hit.collider.gameObject.tag == "SewerFinish")
            {
                interactionCircle.SetActive(true);
                InteractionBtn.SetActive(true);
                Job = "SewerEscape";
                if (Input.GetKeyDown(KeyCode.E))
                {
                    Interact("SewerEscape");
                }
            }
            else
            {
                interactionCircle.SetActive(false);
            }
            #endregion

            // Ray Hits Main Tank
            #region  Tank
            if (hit.collider.gameObject.tag == "Tank")
            {
                if(!hit.collider.gameObject.GetComponent<fuelControl>().isFilled)
                {
                    fillBtn.SetActive(true);   
                }
            }
            #endregion
        } 
        else 
        {
            
            if(itemName.IsActive() == true)
            {
                itemName.gameObject.SetActive(false);
            }

            if(InteractionBtn.activeInHierarchy == true)
            {
                    InteractionBtn.SetActive(false);
            }

        }
        #endregion
        #region Active/Deactive Buttons 
        if(isPicking && dropBtn.activeSelf == false)
        {
            dropBtn.SetActive(true);
        } 
        else if(!isPicking)
        {   
            if (dropBtn.activeSelf == true)
            {
                dropBtn.SetActive(false);
            }
            
        }
        #endregion
        #region PickPC 
        if(isPicking)
        {
            if(Input.GetKey(KeyCode.T))
            {
                DropItem();
            }
        }
        #endregion
        #region HidePC
        if(hideBtn.activeInHierarchy == true)
        {
            if(Input.GetKeyDown(KeyCode.R))
            {
                transform.parent.GetComponent<Player>().HidePlayer();
            }
        }
        #endregion

        #region ShootPC
        if(ShootBtn.activeInHierarchy == true)
        {
            if(Input.GetKeyDown(KeyCode.Q))
            {
                PickedItem.GetComponent<GunControl>().Shoot();
            }
        }
        #endregion
    }

    public void Interact(string job)
    {   
        job = this.Job;
        if(job == "Pick")
        {
            if (!hit.collider.gameObject.GetComponent<objectControl>().isLeftHand)
            {
                if (!isPicking)
                {
                    PickItem(hit.collider.gameObject);
                }
                else
                {
                    DropItem();
                    PickItem(hit.collider.gameObject);
                    // isPicking = true;
                }
            }
            else 
            {
                PickLeftItem(hit.collider.gameObject);
            }
        }

        if(job == "Bullet")
        {
            if(PickedItem.tag == "Gun")
            {
                if(PickedItem.GetComponent<GunControl>().ammo < PickedItem.GetComponent<GunControl>().capacity)
                {
                    PickedItem.GetComponent<GunControl>().ammo++;
                    hit.collider.gameObject.SetActive(false);
                }
                else
                {
                    gameController.instance.DisplayMessage(gameController.instance.messageController.GunFullFinal);
                }
            }
            else
            {
                gameController.instance.DisplayMessage(gameController.instance.messageController.GunFullFinal);
            }
        }

        if(job == "Door")
        {
            hit.collider.gameObject.GetComponent<doorControl>().ChangeDoorState();
        }

        if(job == "Well")
        {
            GameObject bucket = GameObject.Find("Bucket");
            GameObject well = hit.collider.gameObject;
            if(PickedItem == bucket)
            {
                if(bucket.GetComponent<Bucket>().Filled)
                {
                    source.clip = emptyBucketClip;
                    source.Play();
                    well.GetComponent<wellControl>().Fill();
                    bucket.GetComponent<Bucket>().water.SetActive(false);
                    bucket.GetComponent<Bucket>().Filled = false;
                }
            }
            else
            {
                gameController.instance.DisplayMessage(gameController.instance.messageController.WellFinal);
            }
        }

        if(job == "PowerButton")
        {
            hit.collider.gameObject.GetComponent<powerbuttonControl>().isLocked = false;
            hit.collider.gameObject.GetComponent<powerbuttonControl>().PressSound();
        }

        if(job == "KeypadBtn")
        {
            codelockControl codeLock = GameObject.Find("CodeLock").GetComponent<codelockControl>();

            GameObject button = hit.collider.gameObject;
            button.GetComponentInChildren<keypadButton>().PressSound();
            if(codeLock.a == 0)
            {
                codeLock.a = button.GetComponent<keypadButton>().btnValue;
                return;
            }
            
            if(codeLock.a != 0 && codeLock.b == 0)
            {
                codeLock.b = button.GetComponent<keypadButton>().btnValue;
                return;
            }
            if(codeLock.a != 0 && codeLock.b != 0 && codeLock.c == 0)
            {
                codeLock.c = button.GetComponent<keypadButton>().btnValue;
                return;
            }
            if(codeLock.a != 0 && codeLock.b != 0 && codeLock.c != 0 && codeLock.d == 0)
            {
                codeLock.d = button.GetComponent<keypadButton>().btnValue;
                return;
            }
            if(button.GetComponent<keypadButton>().btnValue == 10)
            {
                if(codeLock.a == codeLock.codeA &&
                   codeLock.b == codeLock.codeB &&
                   codeLock.c == codeLock.codeC &&
                   codeLock.d == codeLock.codeD)
                   {
                       codeLock.openLock();
                   }
                   else 
                   {
                       codeLock.resetLock();
                   }
                
            }
        }

        if(job == "Drawer")
        {
            hit.collider.gameObject.GetComponent<drawerControl>().ChangeDoorState();
        }

        if(job == "Locked")
        {
            GameObject other = hit.collider.gameObject;
            if(PickedItem != other.GetComponent<LockInteraction>().requiredItem)
            {
                string displayMessage = "";
                displayMessage = Localization.LanguageJSON[PlayerPrefs.GetString("Language")][ other.GetComponent<LockInteraction>().languageKey];


                gameController.instance.DisplayMessage(displayMessage);
            }
            inventoryItem = PickedItem;
            
            if(other.GetComponent<LockInteraction>(). requiredItem == inventoryItem)
            {
                other.GetComponent<LockInteraction>().locked = false;
                other.GetComponent<LockInteraction>().UnlockObject();
            }
            
        }

        if(job == "Finish")
        {
            if(mainGate.GetComponent<mainGateControl>().CheckItems())
            {
                // Game Complete
                Debug.Log("Game Completed");
               StartCoroutine(gameObject.transform.parent.GetComponent<Player>().gameManager.GetComponent<gameComplete>().finishGame(EndScreenType.Escape01));
            }
            else
            {
                gameController.instance.DisplayMessage(gameController.instance.messageController.LockedDoorFinal);
            }
        }
        if(job == "HeliEscape")
        {


            if(referenceObject.heliEscape.GetComponent<heliEscape>().CheckItems())
            {
                // Game Complete
                Debug.Log("Game Completed");
               StartCoroutine(gameObject.transform.parent.GetComponent<Player>().gameManager.GetComponent<gameComplete>().finishGame(EndScreenType.Escape02));
            }
            else
            {
                gameController.instance.DisplayMessage(gameController.instance.messageController.HeliEscapeFinal);
            }
        }

        if (job == "SewerEscape")
        {


            if (referenceObject.sewerEscape.GetComponent<sewerEscapeController>().CheckItems())
            {
                // Game Complete
                Debug.Log("Game Completed");
                StartCoroutine(gameObject.transform.parent.GetComponent<Player>().gameManager.GetComponent<gameComplete>().finishGame(EndScreenType.Escape03));
            }
            else
            {
                gameController.instance.DisplayMessage(gameController.instance.messageController.SewerEscapeFinal);
            }
        }

    }

    public void PickItem(GameObject item)
    {
        if(item.tag == "Gun")
        {
            ShootBtn.SetActive(true);
        }
        if(item.GetComponent<Rigidbody>() == null)
        {
            item.AddComponent<Rigidbody>();
        }

        if (item.name != "FlashLight")
        {
            PickedItem = item;
        }
        else 
        {
            LeftItem = item;
        }

        if (PickedItem.name == "Code")
        {
            GameObject.Find("KeypadLock").GetComponent<BoxCollider>().enabled = false;
        }

        PickedItem.transform.localScale = PickedItem.transform.localScale / 10f;
        PickedItem.GetComponent<Rigidbody>().useGravity = false;
        PickedItem.GetComponent<Rigidbody>().isKinematic = true;
        PickedItem.transform.parent = pickUpPoint.transform;
        PickedItem.transform.position = pickUpPoint.position;  
        PickedItem.transform.localPosition = PickedItem.GetComponent<objectControl>().pickedPosition;   
        PickedItem.transform.localRotation = Quaternion.Euler(PickedItem.GetComponent<objectControl>().pickedRotation);

        if (item.name != "FlashLight") 
        {
            isPicking = true;
        }
        itemName.gameObject.SetActive(false);
    }

    public void PickLeftItem(GameObject item)
    {
        if (item.GetComponent<Rigidbody>() == null)
        {
            item.AddComponent<Rigidbody>();
        }

        LeftItem = item;

        LeftItem.transform.localScale = LeftItem.transform.localScale / 10f;
        LeftItem.GetComponent<Rigidbody>().useGravity = false;
        LeftItem.GetComponent<Rigidbody>().isKinematic = true;
        LeftItem.transform.parent = pickUpPoint.transform;
        LeftItem.transform.position = pickUpPoint.position;
        LeftItem.transform.localPosition = LeftItem.GetComponent<objectControl>().pickedPosition;
        LeftItem.transform.localRotation = Quaternion.Euler(LeftItem.GetComponent<objectControl>().pickedRotation);

        itemName.gameObject.SetActive(false);
    }

    public void DropItem()
    {
        if(PickedItem.tag == "Gun")
        {
            ShootBtn.SetActive(false);
        }
        PickedItem.transform.localScale = PickedItem.transform.localScale * 10f;
        PickedItem.transform.parent = GameObject.Find("Pickables").transform;
        PickedItem.transform.position = dropPoint.position;
        PickedItem.GetComponent<Rigidbody>().useGravity = true;
        PickedItem.GetComponent<Rigidbody>().isKinematic = false;
        PickedItem = null;
        isPicking = false;
    }

    public void DropLeftItem()
    {
        Debug.Log("Left Item Droped");
        LeftItem.transform.localScale = LeftItem.transform.localScale * 10f;
        LeftItem.transform.parent = GameObject.Find("Pickables").transform;
        LeftItem.transform.position = dropPoint.position;
        LeftItem.GetComponent<Rigidbody>().useGravity = true;
        LeftItem.GetComponent<Rigidbody>().isKinematic = false;
        LeftItem = null;
    }

    public void Shoot()
    {
        
    }

    // public IEnumerator DisplayMessage(string message, float timer)
    // {
    //     hintText.text = message;
    //     hintText.gameObject.SetActive(true);
    //     yield return  new WaitForSeconds(timer);
    //     hintText.gameObject.SetActive(false);
    // }
}
