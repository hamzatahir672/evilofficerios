﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class drawerControl : MonoBehaviour
{
    public bool open;
    public Vector3 openPos;
    public Vector3 closePos;

    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void ChangeDoorState()
    {   
        source.Play();
        open = !open;
    }
    // Update is called once per frame
    void Update()
    {
        // Vector3 targetPosition01 = Quaternion.Euler(0, openAngle, 0);
        // Quaternion targetRotation02 = Quaternion.Euler(0, closeAngle, 0);

        if(open)
        {   
            // if(transform.localPosition == openPos)
            // {
            //     obstacle.carving = true;
            // }
            transform.localPosition = Vector3.Lerp(transform.localPosition,new Vector3(openPos.x,openPos.y,openPos.z) , 12f * Time.deltaTime);
        } 
        else if(!open)
        {
            // if(transform.localPosition == closePos)
            // {
            //     obstacle.carving = false;
            // }
            transform.localPosition = Vector3.Lerp(transform.localPosition,new Vector3(closePos.x, closePos.y, closePos.z), 12f * Time.deltaTime);
        } 

        // if(transform.localPosition != openPos && transform.localPosition != closePos)
        // {
        //     gameObject.GetComponent<BoxCollider>().enabled = false;
        // } 
        // else if(transform.localPosition == openPos || transform.localPosition == closePos)
        // {
        //     gameObject.GetComponent<BoxCollider>().enabled = true;
        // }
    }
}
