﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : MonoBehaviour
{
    [SerializeField] AudioClip[] tileFootSteps;
    [SerializeField] AudioClip[] dirtFootSteps;
    [SerializeField] AudioClip[] woodFootSteps;
    [SerializeField] AudioClip[] exaustedSounds;

    private AudioClip[] currentFootSteps = new AudioClip[7];
    private AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider collision)
    {
            Debug.LogWarning(collision.gameObject.name);

        if (collision.gameObject.CompareTag("TileFloor")) 
        {
            Debug.LogWarning("TileFloor");
            currentFootSteps = tileFootSteps;
        }
        else if (collision.gameObject.CompareTag("WoodFloor"))
        {
            Debug.LogWarning("WoodFloor");
            currentFootSteps = woodFootSteps;
        }
        else if (collision.gameObject.CompareTag("DirtFloor"))
        {
            Debug.LogWarning("DirtFloor");
            currentFootSteps = dirtFootSteps;
        }
    }

    public void Anim_PlayExaustedSound() 
    {
        source.clip = exaustedSounds[Random.Range(0, exaustedSounds.Length)];
        source.Play();
    }

    public void Anim_PlayFootStepSound(int clipIndex) 
    {
        source.clip = currentFootSteps[clipIndex];
        source.Play();
    }

    public void Anim_PlayRandomFootStepSound()
    {
        int clipIndex = Random.Range(0, currentFootSteps.Length);

        source.clip = currentFootSteps[clipIndex];
        source.Play();
    }
}
