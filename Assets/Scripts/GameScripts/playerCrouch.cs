﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityStandardAssets.Characters.FirstPerson;
public class playerCrouch : MonoBehaviour
{
    public bool playerCrouching = false;
    public Button crouchBtn;
    public GameObject playerhead;
    public GameObject player;
    public Sprite standImg;
    public Sprite crouchImg;

    startDay speedController;
    // Start is called before the first frame update
    void Start()
    {
       speedController = GetComponent<startDay>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Crouch();
        }
    }

    public void Crouch(){
        if(!playerCrouching)
        {
            // playerhead.transform.localPosition = Vector3.zero;
            // player.transform.localScale = new Vector3(1f, 0.5f, 1f);
            player.GetComponent<CharacterController>().height = 0f;
            crouchBtn.GetComponent<Image>().sprite = crouchImg;
            player.GetComponent<FirstPersonController>().forwardSpeed = speedController.forwardSpeedCrouch;
            player.GetComponent<FirstPersonController>().backwardSpeed = speedController.backwardSpeedCrouch;
            player.GetComponent<FirstPersonController>().sideSpeed = speedController.sideSpeedCrouch;
            player.GetComponent<Animator>().speed = 0.5f;
            player.GetComponent<AudioSource>().volume = 0.75f;
        }
        if (playerCrouching)
        {
            // playerhead.transform.localPosition = Vector3.up;
            // player.transform.localScale = Vector3.one;
            player.GetComponent<CharacterController>().height = 1.8f;
            crouchBtn.GetComponent<Image>().sprite = standImg;
            player.GetComponent<FirstPersonController>().forwardSpeed = speedController.forwardSpeedStand;
            player.GetComponent<FirstPersonController>().backwardSpeed = speedController.backwardSpeedStand;
            player.GetComponent<FirstPersonController>().sideSpeed = speedController.sideSpeedStand;
            player.GetComponent<Animator>().speed = 1f;
            player.GetComponent<AudioSource>().volume = 1f;
        }

        playerCrouching = !playerCrouching;
    }
}
