﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class extra : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(Upload());
    }

    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();
        form.AddField("a", "acc8396c372e6846b88447e40254a1e13c7a7dde");
        form.AddField("type", "-1");

        using (UnityWebRequest www = UnityWebRequest.Post("https://retrac.xyz/mobile-api/emma/get_alert_type_info.php", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }
}