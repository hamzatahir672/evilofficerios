﻿using System;
using UnityEngine;

// Token: 0x02000077 RID: 119
[Serializable]
public class EnemyEye : MonoBehaviour
{
	
	public LayerMask layerMask;
	public Transform myTransform;
	public Transform target;
	public Camera cam;
	public GameObject granny;
	public GameObject playerHead;
	public float seeRange;
	public GameObject playerCrouch;
	public bool playerInFront = false;
	public float attackDistance;
	public AudioManager audio;

	private int audioIndex = 0;
	private int tempInt;
	int i = 0;


	// Token: 0x06000351 RID: 849 RVA: 0x00015FA0 File Offset: 0x000141A0
	public virtual void Start()
	{
		//int mask1 = 1 << 2;
		//int mask2 = 1 << 11;
		//this.layerMask = mask1 | mask2;
		//this.layerMask = ~this.layerMask;

		if (PlayerPrefs.GetInt("DiffData") == 2)
		{
			this.seeRange = 250f;
		}
		else
		{
			this.seeRange = 200f;
		}
		this.cam = base.GetComponent<Camera>();

	}

	// Token: 0x06000352 RID: 850 RVA: 0x000160C0 File Offset: 0x000142C0
	public virtual void Update()
	{
		if (PlayerPrefs.GetString("Difficulty") != "Ghost")
		{

			RaycastHit raycastHit = default(RaycastHit);
			Vector3 vector = this.cam.WorldToViewportPoint(this.target.position);

			float sqrMagnitude = (this.target.position - this.myTransform.position).sqrMagnitude;

            #region LEGACY
            // Debug.Log(seeRange + " | " + sqrMagnitude);
            //if (((playerCrouch)this.playerCrouch.GetComponent(typeof(playerCrouch))).playerCrouching && ((EnemyAI)this.granny.GetComponent(typeof(EnemyAI))).playerHiding)
            //{
            //	//if (PlayerPrefs.GetInt("DiffData") == 2 || PlayerPrefs.GetInt("DiffData") == 3)
            //	//{
            //	//	this.seeRange = 400f;
            //	//}
            //	//else if (PlayerPrefs.GetInt("DiffData") == 0)
            //	//{
            //	//	this.seeRange = 300f;
            //	//}
            //	//else
            //	//{
            //	//	this.seeRange = 100f;
            //	//}
            //}
            //else if (((playerCrouch)this.playerCrouch.GetComponent(typeof(playerCrouch))).playerCrouching && !((EnemyAI)this.granny.GetComponent(typeof(EnemyAI))).playerHiding)
            //{
            //	//if (PlayerPrefs.GetInt("DiffData") == 2 || PlayerPrefs.GetInt("DiffData") == 3)
            //	//{
            //	//	this.seeRange = 600f;
            //	//}
            //	//else if (PlayerPrefs.GetInt("DiffData") == 0)
            //	//{
            //	//	this.seeRange = 500f;
            //	//}
            //	//else
            //	//{
            //	//	this.seeRange = 150f;
            //	//}
            //}
            //else if (!((playerCrouch)this.playerCrouch.GetComponent(typeof(playerCrouch))).playerCrouching && ((EnemyAI)this.granny.GetComponent(typeof(EnemyAI))).playerHiding)
            //{
            //	if (PlayerPrefs.GetInt("DiffData") == 2 || PlayerPrefs.GetInt("DiffData") == 3)
            //	{
            //		this.seeRange = 600f;
            //	}
            //	else if (PlayerPrefs.GetInt("DiffData") == 0)
            //	{
            //		this.seeRange = 500f;
            //	}
            //	else
            //	{
            //		this.seeRange = 150f;
            //	}
            //}
            //else if (!((playerCrouch)this.playerCrouch.GetComponent(typeof(playerCrouch))).playerCrouching && !((EnemyAI)this.granny.GetComponent(typeof(EnemyAI))).playerHiding)
            //{
            //	if (PlayerPrefs.GetInt("DiffData") == 2 || PlayerPrefs.GetInt("DiffData") == 3)
            //	{
            //		this.seeRange = 800f;
            //	}
            //	else if (PlayerPrefs.GetInt("DiffData") == 0)
            //	{
            //		this.seeRange = 700f;
            //	}
            //	else
            //	{
            //		this.seeRange = 200f;
            //	}
            //}
            #endregion

            i++;

			if (i >= 2)
			{
				i = 0;
				if (Physics.Linecast(this.myTransform.position, this.target.position, out raycastHit, this.layerMask))
				{
					if (raycastHit.collider.gameObject.name == this.target.name)
					{
						transform.parent.gameObject.GetComponent<AudioSource>().volume = 0.12f;
						if (sqrMagnitude < attackDistance * attackDistance)
						{
							if (!target.GetComponent<Player>().hiddenFromEnemy)
							{
								if (PlayerPrefs.GetString("Difficulty") != "Ghost")
								{
									AttackPlayer();
								}
									return;
							}
						}
						playerInFront = true;
						if (sqrMagnitude < this.seeRange)
						{

							if (vector.z <= 0f || vector.x <= 0f || vector.x >= 1f || vector.y <= 0f || vector.y >= 1f)
							{
								((EnemyAI)this.granny.GetComponent(typeof(EnemyAI))).seePlayer = false;
								return;
							}
							else
							{
								if (!target.gameObject.GetComponent<Player>().hiddenFromEnemy)
								{
									((EnemyAI)this.granny.GetComponent(typeof(EnemyAI))).seePlayer = true;
									((EnemyAI)this.granny.GetComponent(typeof(EnemyAI))).seeTimer = 2f;


								}
								return;
							}

						}
					}
					else
					{
						((EnemyAI)this.granny.GetComponent(typeof(EnemyAI))).seePlayer = false;
						playerInFront = false;
						transform.parent.gameObject.GetComponent<AudioSource>().volume = 0.075f;
					}
				}

				if (sqrMagnitude < attackDistance * attackDistance)
				{
					if (!target.GetComponent<Player>().hiddenFromEnemy && target.GetComponent<Player>().isHiding)
					{
                        if (PlayerPrefs.GetString("Difficulty") != "Ghost")
                        {
                            AttackPlayer();
					    }
							return;
					}
				}
			}
		}
		
	}

	void AttackPlayer() 
	{
			transform.parent.GetComponent<EnemyAI>().playerAlive = false;
			if (playerCrouch.GetComponent<playerCrouch>().playerCrouching)
			{
				playerCrouch.GetComponent<playerCrouch>().Crouch();
			}
			if (playerHead.GetComponent<Interaction>().isPicking)
			{
				Debug.Log("Right Item Dropping");
				playerHead.GetComponent<Interaction>().DropItem();
			}
			if (playerHead.GetComponent<Interaction>().LeftItem != null)
			{
				Debug.Log("Left Item Dropping");
				playerHead.GetComponent<Interaction>().DropLeftItem();
			}
	}

}
