﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class LockInteraction : MonoBehaviour
{
        public GameObject requiredItem;
        public bool locked = true;
        public bool disableCollider = false;
        public string objFunction;

    public string languageKey;

        //public string displayMessageEN;
        //public string displayMessageFR;
        //public string displayMessageUR;
        //public string displayMessageHN;
        //public string displayMessageSP;
        //public string displayMessageIT;
        private mainGateControl mainGate;
        private heliEscape helicopter;
        public AudioClip sound;
        [SerializeField] bool playSound;
        [SerializeField] GameObject[] objectsToActive;

        [SerializeField]
        private Transform bucketPoint;
        private ReferenceObj referenceObject;

        void Start()
        {
                referenceObject = GameObject.Find("ReferenceController").GetComponent<ReferenceObj>();
                mainGate = referenceObject.mainGate.GetComponent<mainGateControl>();
                helicopter = referenceObject.heliEscape.GetComponent<heliEscape>();
        }

        public void UnlockObject()
        {
                
                if(mainGate != null)
                {
                        mainGate.CheckItems();
                }
                if(objFunction == "Plank")
                {
                        StartCoroutine(BreakPlank());
                        gameObject.tag = "Untagged";
                }
                if(objFunction == "Door")
                {
                        gameObject.tag = objFunction;
                        gameObject.GetComponent<doorControl>().ChangeDoorState();
                }
                if(objFunction == "Door(Heli)")
                {
                        GameObject heli = GameObject.Find("Helicop");
                        heli.GetComponent<CapsuleCollider>().enabled = true;
                        Debug.Log(heli.gameObject);
                        gameObject.tag = "Door";
                        gameObject.GetComponent<doorControl>().ChangeDoorState();
                }
                if(objFunction == "PlaceGuide")
                {
                        referenceObject = GameObject.Find("ReferenceController").GetComponent<ReferenceObj>();
                        referenceObject.playerHead.GetComponent<Interaction>().DropItem();
                        requiredItem.SetActive(false);
                        referenceObject.heliGuide.SetActive(true);
                        gameObject.tag = "Untagged";
                        
                }
                if(objFunction == "HeliHandle")
                {
                        referenceObject = GameObject.Find("ReferenceController").GetComponent<ReferenceObj>();
                        referenceObject.playerHead.GetComponent<Interaction>().DropItem();

                        Destroy(requiredItem.GetComponent<Rigidbody>());

                        requiredItem.transform.position = transform.position;
                        requiredItem.transform.rotation = transform.rotation;
                        requiredItem.tag = "Untagged";
                        gameObject.tag = "Untagged";
                }
                if(objFunction == "Tap")
                {
                        GameObject bucket = GameObject.Find("Bucket");
                        if(!bucket.GetComponent<Bucket>().Filled)
                        {
                                GameObject obj = referenceObject.playerHead;
                                obj.GetComponent<Interaction>().DropItem();
                                Destroy(bucket.GetComponent<Rigidbody>());
                                bucket.transform.position = bucketPoint.position;
                                bucket.transform.rotation = bucketPoint.rotation;
                                gameObject.GetComponentInChildren<ParticleSystem>().Play();
                                StartCoroutine(FillBucket(bucket));
                        }
                        
                }
                if(objFunction == "Screw")
                {
                        gameObject.AddComponent<Rigidbody>();
                        GameObject.Find("lid").GetComponent<electricboxControl>().openBox();
                        gameObject.tag = "Untagged";
                }

                // Secret Book Placed
                if(objFunction == "Grave")
                {
                        GameObject obj = referenceObject.playerHead;
                        obj.GetComponent<Interaction>().DropItem();
                        Destroy(requiredItem.GetComponent<Rigidbody>());
                        requiredItem.transform.position = new Vector3(-8.34f, 1.8f, 5.095f);
                        requiredItem.transform.rotation = Quaternion.Euler(new Vector3(30f, 90f, 180f));
                        //GameObject gravelock = GameObject.Find("GraveLock");
                        gameObject.GetComponent<LockInteraction>().locked = false;
                        requiredItem.tag = "Untagged";
                        gameObject.tag = "Untagged";
                }
                if(objFunction == "Dig")
                {
                        gameObject.GetComponent<BoxCollider>().enabled = false;
                        gameObject.GetComponent<MeshRenderer>().enabled = false;
                        AudioSource source = GetComponent<AudioSource>();
                        source.Play();
                }
                if(objFunction == "Basic")
                {
                        gameObject.tag = "Untagged";
                }

                if (objFunction == "Active")
                {
                    gameObject.tag = "Untagged";

                    foreach (var obj in objectsToActive)
                    {
                        obj.SetActive(true);
                    }
                }

                if (objFunction == "ActivewithDrop")
                {
                    referenceObject.playerHead.GetComponent<Interaction>().DropItem();
                    requiredItem.SetActive(false);

            foreach (var obj in objectsToActive)
                    {
                        obj.SetActive(true);
                    }

            gameObject.tag = "Untagged";
        }

        //if (objFunction == "Deactive")
        //        {
        //            gameObject.tag = "Untagged";
        //            gameObject.SetActive(false);
        //        }

                if (playSound) 
                {

                    AudioSource source = GetComponent<AudioSource>();
                    if (source == null) 
                    {
                        source = gameObject.AddComponent<AudioSource>();
                    }
                    source.Play();
                }

                if(mainGate != null)
                {
                        mainGate.CheckItems();
                }
                if(helicopter != null)
                {
                        helicopter.CheckItems();
                }
        if (disableCollider) 
        {
            GetComponent<BoxCollider>().enabled = false;
        }
                
        }

        IEnumerator FillBucket(GameObject bucket)
        {
                AudioSource source;
                if(gameObject.GetComponent<AudioSource>() == null)
                {
                        source = gameObject.AddComponent<AudioSource>();
                        source.clip = sound;
                        source.Play();
                }
                else
                {
                        source = gameObject.GetComponent<AudioSource>();
                        source.clip = sound;
                        source.Play();
                }
                
                
                bucket.tag = "Untagged";
                yield return new WaitForSeconds(6f);
                bucket.tag = "Pickable";
                bucket.GetComponent<Bucket>().water.SetActive(true);
                bucket.GetComponent<Bucket>().Filled = true;


        }

        IEnumerator BreakPlank()
        {
                GameObject player = GameObject.Find("Player");
                player.GetComponentInChildren<Interaction>().DropItem();
                requiredItem.transform.parent = gameObject.transform;
                Destroy(requiredItem.GetComponent<Rigidbody>());
                player.GetComponent<FirstPersonController>().enabled = false;
                gameObject.GetComponent<Animation>().Play();
                yield return new WaitForSeconds(2.51f);
                player.GetComponent<FirstPersonController>().enabled = true;
                requiredItem.transform.parent = GameObject.Find("Pickables").transform.parent;
                player.GetComponentInChildren<Interaction>().PickItem(requiredItem);
                gameObject.GetComponent<BoxCollider>().enabled = false;
                
                if(gameObject.GetComponentInChildren<doorControl>() != null)
                {
                        Debug.Log("1");
                        gameObject.GetComponentInChildren<doorControl>().gameObject.tag = "Door";
                }
                // else
                // {
                //         gameObject.GetComponentInChildren<LockInteraction>().locked = false;
                //         gameObject.GetComponentInChildren<LockInteraction>().gameObject.tag = "Untagged";
                // }
        }

        
}
