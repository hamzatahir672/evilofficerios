﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerbuttonControl : MonoBehaviour
{
    public bool isLocked = true;

   [SerializeField] GameObject graveLock;
   [SerializeField] Vector3 unlockPos;



   void Update()
   {
       if(!isLocked)
       {
            graveLock.transform.position = Vector3.Lerp(graveLock.transform.position, unlockPos, 1f * Time.deltaTime);
       }
   }

    public void PressSound()
    {
        GetComponent<AudioSource>().Play();
    }
}
