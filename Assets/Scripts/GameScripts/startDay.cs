﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class startDay : MonoBehaviour
{

    public GameObject controllers;
    public GameObject hideBtn;
    public EnemyAI enemy;
    public scorpionAI scorpion;
    public GameObject player;
    public Vector3 playerHeadPos;
    public Transform playerPoint;    
    public Transform enemyPoint;
    public Transform scorpionPoint;
    public GameObject endScreen;
    public GameObject dayText;

    public float forwardSpeedStand;
    public float backwardSpeedStand;
    public float sideSpeedStand;

    public float forwardSpeedCrouch;
    public float backwardSpeedCrouch;
    public float sideSpeedCrouch;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(startNewDay());
    }

    // Update is called once per frame
  
    public IEnumerator startNewDay()
    {  
        Rigidbody[] bones = enemy.gameObject.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody bone in bones)
        {
            bone.isKinematic = true;
        }
        enemy.gameObject.GetComponent<Animator>().enabled = true;
        scorpion.gameObject.GetComponent<Animator>().enabled = true;

        player.GetComponentInChildren<Camera>().gameObject.transform.localPosition = playerHeadPos;
        player.GetComponent<FirstPersonController>().enabled = true;
        player.GetComponent<FirstPersonController>().mouseLook.m_CameraTargetRot = Quaternion.Euler(Vector3.zero);
        player.GetComponent<FirstPersonController>().mouseLook.m_CharacterTargetRot = Quaternion.Euler(Vector3.zero);
        player.GetComponent<FirstPersonController>().RunAxis = Vector2.zero;

        enemy.playerAlive = true;
        scorpion.playerAlive = true;

        gameObject.GetComponent<playerCaught>().deathPlayed = false;
        // endScreen.SetActive(false);
        dayText.SetActive(true);
        player.transform.position = playerPoint.position;

        enemy.gameObject.transform.position = enemyPoint.position;
        enemy.gameObject.GetComponent<NavMeshAgent>().enabled = true;
        enemy.seeTimer = 0f;
        enemy.state = "idle";

        if (scorpion.isAlive) 
        {
            scorpion.ChangeState("idle");
            scorpion.gameObject.transform.position = scorpionPoint.position;
            scorpion.gameObject.GetComponent<NavMeshAgent>().enabled = true;
        
        }

        player.GetComponent<CharacterController>().enabled = true;
        player.GetComponent<Animation>().Play();
        hideBtn.SetActive(false);
        
        yield return new WaitForSeconds(6f);

        dayText.SetActive(false);

        if (PlayerPrefs.GetString("GameMode") == "Nightmare") 
        {
            int random = Random.Range(0, 5);

            if(random == 0)
                gameController.instance.DisplayMessage("It's too dark. I need to find a torch");
            else if (random == 1)
                gameController.instance.DisplayMessage("I need to find torch");
            else if (random == 2)
                gameController.instance.DisplayMessage("I need a flashlight.");
            else if (random == 3)
                gameController.instance.DisplayMessage("A torchlight can be really helpful");
            else if (random == 4)
                gameController.instance.DisplayMessage("I dropped the torch. I need to find it.");

        }

        player.transform.rotation = playerPoint.rotation;
        controllers.SetActive(true);

        enemy.canMove = true;
        scorpion.canMove = true;

        player.GetComponentInChildren<Transform>().rotation = Quaternion.Euler(Vector3.zero);
        player.GetComponent<CharacterController>().enabled = true;
        player.GetComponent<FirstPersonController>().forwardSpeed = forwardSpeedStand;
        player.GetComponent<FirstPersonController>().backwardSpeed = backwardSpeedStand;
        player.GetComponent<FirstPersonController>().sideSpeed = sideSpeedStand;
    }
}
