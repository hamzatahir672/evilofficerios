﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectControl : MonoBehaviour
{
    public string languageKey;

    public AudioClip fallSound;
    public Vector3 pickedPosition;
    public Vector3 pickedRotation;
    public bool isLeftHand = false;

    private AudioSource source;
    private ReferenceObj referenceObject;
    private GameObject Enemy;
    // Start is called before the first frame update
    void Start()
    {
        source = gameObject.GetComponent<AudioSource>();
        referenceObject = GameObject.Find("ReferenceController").GetComponent<ReferenceObj>();
        Enemy = referenceObject.EnemyObj;
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Floor")
        {
            source.clip = fallSound;
            source.Play();
            Enemy.GetComponent<EnemyAI>().heardObjectPos = transform;
            Enemy.GetComponent<EnemyAI>().state = "heard";
        }
        if(other.gameObject.tag == "Hole")
        {
            source.clip = fallSound;
            source.Play();
        }
    }
}
