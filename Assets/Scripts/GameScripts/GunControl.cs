﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class GunControl : MonoBehaviour
{
    public int ammo = 0;
    public int capacity = 2;
    public AudioClip fireClip;
    [SerializeField] GameObject hintText;
    [SerializeField] GameObject shootBtn;
    GameObject Enemy;
    GameObject Scorpion;
    GameObject enemyPoint;
    GameObject playerHead;
    AudioSource source;
    int layermask;
    public LayerMask layermask2;


    void Awake()
    {
        Enemy = GameObject.Find("enemy");
        Scorpion = GameObject.Find("scorpion");
        enemyPoint = GameObject.Find("EnemyPoint");
        playerHead = GameObject.Find("_Head");
        int mask1 = 1 << 12;
        layermask = mask1;
        layermask = ~layermask;
        source = gameObject.GetComponent<AudioSource>();
    }

    public void Shoot()
    {
        if(ammo <= 0)
        {
            // hintText.GetComponent<Text>().text = "";
            gameController.instance.DisplayMessage(gameController.instance.messageController.NeedAmmoFinal);
            // StartCoroutine(Timer(hintText,2f));
        }
        else
        {
            RaycastHit hit;
            ammo--;
            StartCoroutine(Timer(shootBtn,0.5f,true));
            source.clip = fireClip;
            source.Play();
            if(Physics.Raycast(playerHead.transform.position,playerHead.transform.forward, out hit, 100f, layermask2))
            {
                gameObject.GetComponent<Animation>().Play();
                if(hit.collider.gameObject.tag == "Enemy")
                {
                    Debug.Log("hit enemy");
                    StartCoroutine(KillEnemy());
                }
                else if (hit.collider.gameObject.tag == "Scorpion")
                {
                    Debug.Log("hit scorpion!!");
                    StartCoroutine(KillScorpion());
                }
                else
                {
                    Debug.Log("hit " + hit.collider.gameObject.name);
                    
                }
            }
        
        }
    } 

    IEnumerator Timer(GameObject obj, float timer,bool invert = false)
    {
        if(invert == false)
        {
            obj.SetActive(true);
            yield return  new WaitForSeconds(timer);
            obj.SetActive(false);
        } 
        if(invert == true)
        {
            obj.SetActive(false);
            yield return  new WaitForSeconds(timer);
            obj.SetActive(true);
        }
        
    } 

    IEnumerator KillEnemy()
    {
        AudioManager.instance.StopClip();
        Enemy.GetComponent<EnemyAI>().audioIndex = 0;
        Enemy.GetComponent<EnemyAI>().seeTimer = 0f;
        Enemy.GetComponent<EnemyAI>().state = "idle";
        yield return null;
        Enemy.GetComponent<EnemyAI>().enabled = false;
        Enemy.GetComponent<Animator>().enabled = false;
        Rigidbody[] bones = Enemy.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody bone in bones)
        {
            bone.isKinematic = false;
        }

        Enemy.GetComponent<NavMeshAgent>().enabled = false;
        Enemy.GetComponentInChildren<EnemyEye>().enabled = false;
        Enemy.GetComponentInChildren<Animator>().SetBool("dead", true);
        string txt = gameController.instance.messageController.InspectGoneFinal;
        float time = 0;
        if (PlayerPrefs.GetString("Difficulty") == "Easy" || PlayerPrefs.GetString("Difficulty") == "Normal") 
        {
            txt = txt.Replace("30" , "90");
            
            time = 85;
        }
        else if (PlayerPrefs.GetString("Difficulty") == "Hard")
        {
            txt = txt.Replace("30", "60");
            time = 55;
        }
        else if (PlayerPrefs.GetString("Difficulty") == "Extreme")
        {
            txt = txt.Replace("30", "45");
            time = 40;
        }

        gameController.instance.DisplayMessage(txt);
        yield return  new WaitForSeconds(5f);
        Enemy.SetActive(false);

        yield return  new WaitForSeconds(time);
        Enemy.transform.position = enemyPoint.transform.position;
        foreach (Rigidbody bone in bones)
        {
            bone.isKinematic = true;
        } 
        Enemy.GetComponent<Animator>().enabled = true;
        Enemy.SetActive(true);
        Enemy.GetComponentInChildren<EnemyEye>().enabled = true;
        Enemy.GetComponent<NavMeshAgent>().enabled = true;
        Enemy.GetComponent<EnemyAI>().enabled = true;
    }

    IEnumerator KillScorpion()
    {
        //AudioManager.instance.StopClip();
        //Scorpion.GetComponent<scorpionAI>().audioIndex = 0;
        //Enemy.GetComponent<EnemyAI>().seeTimer = 0f;
        if (Scorpion.GetComponent<scorpionAI>().isAlive) 
        {
            Scorpion.GetComponent<scorpionAI>().ChangeState("idle");
            yield return null;

            Scorpion.GetComponent<scorpionAI>().DeathSound();
            Scorpion.GetComponent<scorpionAI>().isAlive = false;
            Scorpion.GetComponent<scorpionAI>().enabled = false;
            Scorpion.GetComponent<Animator>().SetTrigger("shot");
            //Enemy.GetComponent<Animator>().enabled = false;
            //Rigidbody[] bones = Enemy.GetComponentsInChildren<Rigidbody>();
            //foreach (Rigidbody bone in bones)
            //{
            //    bone.isKinematic = false;
            //}

            Scorpion.GetComponent<NavMeshAgent>().enabled = false;
        }
        
        //Enemy.GetComponentInChildren<EnemyEye>().enabled = false;
        //Enemy.GetComponentInChildren<Animator>().SetBool("dead", true);
        //gameController.instance.DisplayMessage("Inspector is gone for 30 seconds!");
        //yield return new WaitForSeconds(5f);
        //Enemy.SetActive(false);
        //yield return new WaitForSeconds(25f);
        //Enemy.transform.position = enemyPoint.transform.position;
        //foreach (Rigidbody bone in bones)
        //{
        //    bone.isKinematic = true;
        //}
        //Enemy.GetComponent<Animator>().enabled = true;
        //Enemy.SetActive(true);
        //Enemy.GetComponentInChildren<EnemyEye>().enabled = true;
        //Enemy.GetComponent<NavMeshAgent>().enabled = true;
        //Enemy.GetComponent<EnemyAI>().enabled = true;
    }
}
