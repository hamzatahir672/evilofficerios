﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CameraPPController : MonoBehaviour
{
    Volume ppVolume;

    // Start is called before the first frame update
    void Start()
    {
        ppVolume = GetComponent<Volume>();

        SetCameraPP(PlayerPrefs.GetInt("Quality", 0) > 1 ? true : false);
    }

    public void SetCameraPP(bool state) 
    {
        ppVolume.enabled = state;
    }
}
