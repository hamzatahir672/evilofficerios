﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sewerEscapeController : MonoBehaviour
{

    public LockInteraction requiredItem01;
    public LockInteraction requiredItem02;
    public LockInteraction requiredItem03;
    public LockInteraction requiredItem04;

    public bool isLocked = true;

    //When the player interacts with this item
    public bool CheckItems()
    {
        if (
            !requiredItem01.locked && !requiredItem02.locked && !requiredItem03.locked && !requiredItem04.locked
          )
        {
            isLocked = false;
            return true;
        }
        return false;
    }

}
