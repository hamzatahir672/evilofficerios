﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class playerCaught : MonoBehaviour
{
    public static playerCaught instance;

    public EnemyAI enemy;
    public scorpionAI scorpion;
    public GameObject player, enemyFace;
    public GameObject endText, endScreen, controller;
    public Joystick joystick;
    public Text dayText;
    public GameObject[] deathScenes;
    public GameObject canvasPlayer;
    public AudioManager audioManager;
    public int NoOfDays = 5;
    public bool deathPlayed = false;
    public GameObject newDayDialog;
    public doorControl cellDoor01;
    public doorControl cellDoor02;
    public doorControl enemyDoor;
    public AdsManager adManager;

    private AudioSource source;
    private float currentDay = 1;
    private GameObject playerHead;
    private bool extraDayPlayed = false;

    [SerializeField] bool allowExtraDay;

    // Start is called before the first frame update
    void Start()
    {
        extraDayPlayed = true; // Do not allow extraday
        if (allowExtraDay) 
        {
            AllowExtraDay();
        }

        source = GetComponent<AudioSource>();
        playerHead = player.GetComponentInChildren<Camera>().gameObject;
    }

    void AllowExtraDay() 
    {
        extraDayPlayed = false;
    }

    public void GiveExtraDay() 
    {
        if (PlayerPrefs.GetInt("ZomboCoins") >= 80) 
        {
            int coins = PlayerPrefs.GetInt("ZomboCoins");
            coins -= 80;
            PlayerPrefs.SetInt("ZomboCoins", coins);
            newDayDialog.SetActive(false);
            StartNewDay(6, true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!enemy.playerAlive)
        {
            if (!deathPlayed)
            {
                audioManager.StopClip();
                StartCoroutine(playDeathScene());
                deathPlayed = true;
            }
        }
        else if (!scorpion.playerAlive) 
        {
            if (!deathPlayed)
            {
                audioManager.StopClip();
                StartCoroutine(playScorpionDeathScene());
                deathPlayed = true;
            }
        }
    }

    IEnumerator playDeathScene()
    {  
        source.Play();
        
        enemy.gameObject.GetComponent<Animator>().SetBool("death",true);
        
        player.GetComponent<FirstPersonController>().forwardSpeed = 0;
        player.GetComponent<FirstPersonController>().backwardSpeed = 0;
        player.GetComponent<FirstPersonController>().sideSpeed = 0;
        player.GetComponent<CharacterController>().enabled = false;
        player.GetComponent<FirstPersonController>().enabled = false;

        player.transform.rotation = Quaternion.Euler(Vector3.zero);
        // playerHead.transform.rotation = Quaternion.Euler(Vector3.zero);

        player.transform.LookAt(new Vector3(enemy.gameObject.transform.position.x,
                                            player.transform.position.y,
                                            enemy.gameObject.transform.position.z));

        enemy.gameObject.transform.LookAt(new Vector3(player.transform.position.x,
                                                      enemy.gameObject.transform.position.y,
                                                      player.transform.position.z));

        // Vector3 lookVector01 = enemy.gameObject.transform.position - player.transform.position;
        // lookVector01.y = player.transform.position.y;
        // Quaternion rot01 = Quaternion.LookRotation(enemyFace.transform.position);
        // player.transform.rotation = Quaternion.Euler(new Vector3(rot01.x, rot01.y, rot01.z));

        // Quaternion rot02 = Quaternion.LookRotation(new Vector3(player.transform.position.x, enemy.gameObject.transform.position.y, player.transform.position.z));
        // enemy.gameObject.transform.rotation = rot02;

        enemy.speed = 0;
        enemy.canMove = false;
        enemy.gameObject.GetComponent<NavMeshAgent>().enabled = false;

        scorpion.speed = 0;
        scorpion.canMove = false;
        scorpion.gameObject.GetComponent<NavMeshAgent>().enabled = false;

        controller.SetActive(false);
        joystick.input = Vector2.zero;
        joystick.Start();
        yield return new WaitForSeconds(0.8f);
        // player.GetComponent<Animation>().Play("hitDanda");

        yield return new WaitForSeconds(3f);

        endScreen.SetActive(true);
        playerHead.transform.rotation = Quaternion.Euler(Vector3.zero);

        yield return new WaitForSeconds(2f);
        StartNewDay();
        
    }

    IEnumerator playScorpionDeathScene()
    {
        //source.Play();

        scorpion.anim.SetBool("death", true);

        player.GetComponent<FirstPersonController>().forwardSpeed = 0;
        player.GetComponent<FirstPersonController>().backwardSpeed = 0;
        player.GetComponent<FirstPersonController>().sideSpeed = 0;
        player.GetComponent<CharacterController>().enabled = false;
        player.GetComponent<FirstPersonController>().enabled = false;

        player.transform.rotation = Quaternion.Euler(Vector3.zero);
        // playerHead.transform.rotation = Quaternion.Euler(Vector3.zero);

        player.transform.LookAt(new Vector3(scorpion.gameObject.transform.position.x,
                                            player.transform.position.y,
                                            scorpion.gameObject.transform.position.z));

        scorpion.gameObject.transform.LookAt(new Vector3(player.transform.position.x,
                                                      scorpion.gameObject.transform.position.y,
                                                      player.transform.position.z));

        // Vector3 lookVector01 = enemy.gameObject.transform.position - player.transform.position;
        // lookVector01.y = player.transform.position.y;
        // Quaternion rot01 = Quaternion.LookRotation(enemyFace.transform.position);
        // player.transform.rotation = Quaternion.Euler(new Vector3(rot01.x, rot01.y, rot01.z));

        // Quaternion rot02 = Quaternion.LookRotation(new Vector3(player.transform.position.x, enemy.gameObject.transform.position.y, player.transform.position.z));
        // enemy.gameObject.transform.rotation = rot02;

        enemy.speed = 0;
        enemy.canMove = false;
        enemy.gameObject.GetComponent<NavMeshAgent>().enabled = false;

        scorpion.speed = 0;
        scorpion.canMove = false;
        scorpion.gameObject.GetComponent<NavMeshAgent>().enabled = false;

        controller.SetActive(false);
        joystick.input = Vector2.zero;
        joystick.Start();
        yield return new WaitForSeconds(0.8f);
        // player.GetComponent<Animation>().Play("hitDanda");

        yield return new WaitForSeconds(3f);

        endScreen.SetActive(true);
        playerHead.transform.rotation = Quaternion.Euler(Vector3.zero);

        yield return new WaitForSeconds(2f);
        StartNewDay();

    }

    public void StartNewDay(int _noOfDays = 5, bool _IsExtraDay = false) 
    {
        if (cellDoor01.open) 
        {
            cellDoor01.ChangeDoorState();
        }

        if (cellDoor02.open)
        {
            cellDoor02.ChangeDoorState();
        }

        if (enemyDoor.open)
        {
            enemyDoor.ChangeDoorState();
        }

        if (_IsExtraDay) 
        {
            extraDayPlayed = true;
        }

        if (currentDay < _noOfDays)
        {
            currentDay++;
            string dayMessage = "";

            if (currentDay < 5)
            {
                dayMessage = "Day " + currentDay;
                if (currentDay == 2 || currentDay == 3 || currentDay == 4) 
                {
                    if (PlayerPrefs.GetString("NoAds", "0") == "0") 
                    {
                        //adManager.ShowInterstitialAd();
                    }
                }
            }
            else if (currentDay == 5)
            {
                if (PlayerPrefs.GetString("NoAds", "0") == "0")
                {
                    //adManager.ShowInterstitialAd();
                }
                dayMessage = "Day 5 <size=50>(Last Day)</size>";
            }
            else if (currentDay == 6)
            {
                dayMessage = "Day 6 <size=50>(Bonus Day)</size>";
            }

            dayText.text = dayMessage;
            StartCoroutine(gameObject.GetComponent<startDay>().startNewDay());
        }
        else
        {
            if (extraDayPlayed)
            {
                Death();
            }
            else 
            {
                //Set Active Ad Panel
                newDayDialog.SetActive(true);
            }

        }
    }

    public void Death() 
    {
        canvasPlayer.SetActive(false);
        int i = Random.Range(0, 100);

        if (i <= 40)
        {
            deathScenes[0].SetActive(true);
        }
        else if (i > 40) 
        {
            deathScenes[1].SetActive(true);
        }
    }
}
