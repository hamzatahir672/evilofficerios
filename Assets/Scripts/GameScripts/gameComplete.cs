﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameComplete : MonoBehaviour
{

    public GameObject Enemy;
    public GameObject player;
    public GameObject canvasPlayer;

    public GameObject doorEscapeScene;
    public GameObject heliEscapeScene;
    public GameObject sewerEscapeScene;

    public IEnumerator finishGame(EndScreenType endScreen)
    {
        Enemy.GetComponent<EnemyAI>().canMove = false;
        canvasPlayer.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        RenderSettings.fogDensity = 0.2f;
        Enemy.SetActive(false);
        player.SetActive(false);

        if (endScreen == EndScreenType.Escape01) 
        {
            doorEscapeScene.SetActive(true);
        }
        else if (endScreen == EndScreenType.Escape02)
        {
            heliEscapeScene.SetActive(true);
        }
        else if (endScreen == EndScreenType.Escape03)
        {
            sewerEscapeScene.SetActive(true);
        }
    }
}
