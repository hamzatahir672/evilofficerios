﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keypadButton : MonoBehaviour
{
    public int btnValue;

    AudioSource source; 

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void PressSound()
    {
        source.Play();
    }
}
