﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class musicControl : MonoBehaviour
{

    public AudioClip backgroundClip;
    AudioSource source;
    void Start()
    {
        source = gameObject.GetComponent<AudioSource>();
        source.clip = backgroundClip;
        source.Play();

    }

}
