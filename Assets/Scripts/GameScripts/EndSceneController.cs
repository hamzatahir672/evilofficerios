﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSceneController : MonoBehaviour
{
    public GameObject[] objsToDeactive;
    public GameObject[] objsToActive;
    
    public EndScreenType endScreen;

    // Start is called before the first frame update
    private void OnEnable()
    {
        if (PlayerPrefs.GetString("GameMode") == "Classic") 
        {
            RenderSettings.ambientLight = new Color(1, 1, 1);
        }
        else if(PlayerPrefs.GetString("GameMode") == "Nightmare")
        {
            RenderSettings.ambientLight = new Color(1, 1, 1);
        }

        foreach (var obj in objsToDeactive)
        {
            obj.SetActive(false);
        }

        foreach (var obj in objsToActive)
        {
            obj.SetActive(true);
        }

        if (endScreen == EndScreenType.Escape02) 
        {
            HeliEscape();
        }
    }

    void HeliEscape() 
    {
        RenderSettings.fogDensity = 0.1f;
    }


}
public enum EndScreenType { Death01, Escape01, Escape02, Death02, Escape03 }
