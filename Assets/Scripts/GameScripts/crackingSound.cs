﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crackingSound : MonoBehaviour
{

    public AudioClip cracking;
    public GameObject Enemy;

    private AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
        source = gameObject.GetComponent<AudioSource>();
    }

    public void playCracking()
    {
        Debug.Log("played");
        source.clip = cracking;
        source.Play();
        Enemy.GetComponent<EnemyAI>().heardObjectPos = transform;
        Enemy.GetComponent<EnemyAI>().state = "heard";
    }
   


}
