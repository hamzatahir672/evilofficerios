﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endSound : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip[] footSound;
    public AudioSource source;

    public void footSteps(int clip)
    {
        source.clip = footSound[clip];
        source.Play();
    }
}
