﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeTransition : MonoBehaviour
{
    [SerializeField] float transitionSpeed = 10f;
    
    private Image transitionImage;

    // Start is called before the first frame update
    void Start()
    {
        transitionImage = GetComponent<Image>();
    }

    public void FadeIn(System.Action onTransitionComplete = null) 
    {
        StartCoroutine(coroutine_FadeIn(onTransitionComplete));
    }



    public void FadeOut(System.Action onTransitionComplete = null) 
    {
        StartCoroutine(coroutine_FadeOut(onTransitionComplete));
    }

    IEnumerator coroutine_FadeIn(System.Action onTransitionComplete) 
    {
        transitionImage.raycastTarget = true;
        transitionImage.color = new Color(transitionImage.color.r, transitionImage.color.g, transitionImage.color.b, 0);

        while (transitionImage.color.a < 0.99f) 
        {
            transitionImage.color = new Color(transitionImage.color.r, transitionImage.color.g, transitionImage.color.b, transitionImage.color
                .a + transitionSpeed * Time.deltaTime);
            yield return null;
        }

        if(onTransitionComplete != null)
            onTransitionComplete.Invoke();
    }

    IEnumerator coroutine_FadeOut(System.Action onTransitionComplete) 
    {
        transitionImage.raycastTarget = true;
        transitionImage.color = new Color(transitionImage.color.r, transitionImage.color.g, transitionImage.color.b, 1f);

        while (transitionImage.color.a > 0.01f)
        {
            transitionImage.color = new Color(transitionImage.color.r, transitionImage.color.g, transitionImage.color.b, transitionImage.color
                .a - transitionSpeed * Time.deltaTime);
            yield return null;
        }

        transitionImage.raycastTarget = false;

        if (onTransitionComplete != null)
            onTransitionComplete.Invoke();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
